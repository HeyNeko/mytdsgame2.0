// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Character/MyTDSGame_HealthComponent.h"

#include "MyMyTDSGame_CharHealthComponent.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnShieldDestroyed);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnShieldDamaged);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnShieldRestored);

UCLASS()
class MYTDSGAME_API UMyMyTDSGame_CharHealthComponent : public UMyTDSGame_HealthComponent
{
	GENERATED_BODY()

public:
//Broadcasts
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldChange OnShieldChange;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldDestroyed OnShieldDestroyed;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldDamaged OnShieldDamaged;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldRestored OnShieldRestored;

	//Timers
	FTimerHandle TimerHandle_ShieldCooldownTimer;

	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

protected:
	bool bShieldReady = true;

	float Shield = 100.0f;

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CoolDownShieldRecoverTime = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverValue = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverRate = 0.1f;

	void ChangeHealthValue(float ChangeValue) override;

	UFUNCTION(BlueprintCallable)
	float GetCurrentShield();

	void ChangeShieldValue(float ChangeValue);

	void CoolDownShieldEnd();

	void ShieldRecovery();

	float MaxShield = 100.0f;
};
