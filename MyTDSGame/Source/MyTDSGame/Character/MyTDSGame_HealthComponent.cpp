// Fill out your copyright notice in the Description page of Project Settings.

#include "MyTDSGame_HealthComponent.h"



// Sets default values for this component's properties
UMyTDSGame_HealthComponent::UMyTDSGame_HealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UMyTDSGame_HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UMyTDSGame_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UMyTDSGame_HealthComponent::GetCurrentHealth()
{
	return Health;
}

void UMyTDSGame_HealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UMyTDSGame_HealthComponent::ChangeMaxHealthValue(float NewMaxHealth)
{
	MaxHealth += NewMaxHealth;
}

void UMyTDSGame_HealthComponent::ChangeHealthValue(float ChangeValue)
{
		ChangeValue = ChangeValue * DamageCoef;

		if (ChangeValue != 0)
		{
			Health += ChangeValue;

			OnHealthChange.Broadcast(Health, ChangeValue);

			if (Health > MaxHealth)
			{
				Health = MaxHealth;
			}

			if (Health <= 0.0f)
			{
				OnDead.Broadcast();
			}
		}
}
