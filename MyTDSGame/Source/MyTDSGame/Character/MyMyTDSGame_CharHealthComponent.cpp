// Fill out your copyright notice in the Description page of Project Settings.


#include "MyMyTDSGame_CharHealthComponent.h"

void UMyMyTDSGame_CharHealthComponent::ChangeHealthValue(float ChangeValue)
{

	float CurrentDamage = ChangeValue * DamageCoef;

		if (Shield > 0.0f && ChangeValue < 0.0f && bShieldReady && CurrentDamage != 0)
		{
			ChangeShieldValue(CurrentDamage);
				if (Shield < 0)
				{
					//FX
					UE_LOG(LogTemp, Warning, TEXT("UMyMyTDSGame_CharHealthComponent::ChangeHealthValue - Shield < 0"));
				}
		}
		else
		{
			Super::ChangeHealthValue(ChangeValue);
		}
}

float UMyMyTDSGame_CharHealthComponent::GetCurrentShield()
{
	return Shield;
}

void  UMyMyTDSGame_CharHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield = Shield + ChangeValue;

	OnShieldChange.Broadcast(Shield, ChangeValue);
	 
	if (Shield > 0)
	{
		OnShieldDamaged.Broadcast();
	}


	if(Shield > MaxShield)
	{
		Shield = MaxShield;
	}
	else
	{
		if (Shield <= 0.0f)
		{
			Shield = 0.0f;
			OnShieldDestroyed.Broadcast();
			bShieldReady = false;
		}
	}

	if (GetWorld() && Shield < MaxShield)
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldCooldownTimer, this, &UMyMyTDSGame_CharHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}

}

void UMyMyTDSGame_CharHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld()) 
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UMyMyTDSGame_CharHealthComponent::ShieldRecovery, ShieldRecoverRate, true);

	}

}

 void UMyMyTDSGame_CharHealthComponent::ShieldRecovery()
 {
	float tmp = Shield;
	tmp = tmp + ShieldRecoverValue;
	if (tmp > 100)
	{
		Shield = MaxShield;
		bShieldReady = true;

		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}

		OnShieldRestored.Broadcast();
	}
	else
	{
		Shield = tmp;
	}

	OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
}