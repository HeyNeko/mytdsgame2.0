 // Copyright Epic Games, Inc. All Rights Reserved.
 // Copyright Epic Games, Inc. All Rights Reserved.

#include "MyTDSGameCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "../Game/MyTDSGameInstance.h"
#include "../Game/ProjectileDefault.h"


AMyTDSGameCharacter::AMyTDSGameCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 140.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	//Inventory
	InventoryComponent = CreateDefaultSubobject <UMyTDSGame_InventoryComponent>(TEXT("InventoryComponent"));

	//Health
	CharacterHealthComponent = CreateDefaultSubobject<UMyMyTDSGame_CharHealthComponent>(TEXT("CharacterHealthComponent"));

	if (CharacterHealthComponent)
	{
		CharacterHealthComponent->OnDead.AddDynamic(this, &AMyTDSGameCharacter::CharDead);
	}

	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &AMyTDSGameCharacter::InitWeapon);
	}

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

//������� ����
void AMyTDSGameCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (bIsAlive && !bIStuned)
	{
		if (CurrentCursor)
		{
			APlayerController* myPC = Cast<APlayerController>(GetController());
			if (myPC)
			{
				FHitResult TraceHitResult;
				myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
				FVector CursorFV = TraceHitResult.ImpactNormal;
				FRotator CursorR = CursorFV.Rotation();

				CurrentCursor->SetWorldLocation(TraceHitResult.Location);
				CurrentCursor->SetWorldRotation(CursorR);
			}
		}
	}
	

	MovementTick(DeltaSeconds);
}

//����� ����
void AMyTDSGameCharacter::BeginPlay()
{
	Super::BeginPlay();

	//InitWeapon(InitWeaponName);

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

//��������� ���������� ����������
void AMyTDSGameCharacter::SetupPlayerInputComponent(UInputComponent* InputComponentNew)
{

	Super::SetupPlayerInputComponent(InputComponent);

	//������������
	InputComponentNew->BindAxis(TEXT("MoveForward"), this, &AMyTDSGameCharacter::InputAxisX);
	InputComponentNew->BindAxis(TEXT("MoveRight"), this, &AMyTDSGameCharacter::InputAxisY);

	//�������� � �����������
	InputComponentNew->BindAction(TEXT("InputAttack"), EInputEvent::IE_Pressed, this, &AMyTDSGameCharacter::InputAttackPressed);
	InputComponentNew->BindAction(TEXT("InputAttack"), EInputEvent::IE_Released, this, &AMyTDSGameCharacter::InputAttackReleased);
	InputComponentNew->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &AMyTDSGameCharacter::TryReloadWeapon);

	//������������ ������
	InputComponentNew->BindAction(TEXT("PreviousWeapon"), EInputEvent::IE_Pressed, this, &AMyTDSGameCharacter::TrySwitchPreviousWeapon);
	InputComponentNew->BindAction(TEXT("NextWeapon"), EInputEvent::IE_Pressed, this, &AMyTDSGameCharacter::TrySwitchNextWeapon);

	//������������� ������������
	InputComponentNew->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &AMyTDSGameCharacter::TryAbilityEnabled);
	InputComponentNew->BindAction(TEXT("AbilityAction2"), EInputEvent::IE_Pressed, this, &AMyTDSGameCharacter::TryAbilityEnabled2);
	InputComponentNew->BindAction(TEXT("AbilityAction3"), EInputEvent::IE_Pressed, this, &AMyTDSGameCharacter::TryAbilityEnabled3);
	InputComponentNew->BindAction(TEXT("AbilityAction4"), EInputEvent::IE_Pressed, this, &AMyTDSGameCharacter::TryAbilityEnabled4);
	InputComponentNew->BindAction(TEXT("AbilityAction5"), EInputEvent::IE_Pressed, this, &AMyTDSGameCharacter::TryAbilityEnabled5);

	//������������� � ���������
	InputComponentNew->BindAction(TEXT("Drop"), EInputEvent::IE_Pressed, this, &AMyTDSGameCharacter::DropCurrentWeapon);

	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);

	InputComponentNew->BindKey(HotKeys[1], IE_Pressed, this, &AMyTDSGameCharacter::TKeyPressed<1>);
	InputComponentNew->BindKey(HotKeys[2], IE_Pressed, this, &AMyTDSGameCharacter::TKeyPressed<2>);
	InputComponentNew->BindKey(HotKeys[3], IE_Pressed, this, &AMyTDSGameCharacter::TKeyPressed<3>);
	InputComponentNew->BindKey(HotKeys[4], IE_Pressed, this, &AMyTDSGameCharacter::TKeyPressed<4>);
	InputComponentNew->BindKey(HotKeys[5], IE_Pressed, this, &AMyTDSGameCharacter::TKeyPressed<5>);
	InputComponentNew->BindKey(HotKeys[6], IE_Pressed, this, &AMyTDSGameCharacter::TKeyPressed<6>);
	InputComponentNew->BindKey(HotKeys[7], IE_Pressed, this, &AMyTDSGameCharacter::TKeyPressed<7>);
	InputComponentNew->BindKey(HotKeys[8], IE_Pressed, this, &AMyTDSGameCharacter::TKeyPressed<8>);
	InputComponentNew->BindKey(HotKeys[9], IE_Pressed, this, &AMyTDSGameCharacter::TKeyPressed<9>);
	InputComponentNew->BindKey(HotKeys[0], IE_Pressed, this, &AMyTDSGameCharacter::TKeyPressed<0>);
}

void AMyTDSGameCharacter::InputAxisX(float value)
{
	if (AtSprintState == false)
	{
		AxisX = value;
	}
}

void AMyTDSGameCharacter::InputAxisY(float value)
{
	if (AtSprintState == false)
	{
		AxisY = value;
	}
}

void AMyTDSGameCharacter::InputAttackPressed()
{
	if (bIsAlive)
	{
		AttackCharEvent(true);
	}
}

void AMyTDSGameCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void AMyTDSGameCharacter::MovementTick(float DeltaTime)
{
	if (bIsAlive && !bIStuned)
	{
		if (AtSprintState == false)
		{
			AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX, false);
			AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY, false);
		}

		APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

		if (MyController)
		{
			FHitResult ResultOfHit;
			//MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultOfHit);
			MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultOfHit);

			auto YawDirection = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultOfHit.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, YawDirection, 0.0f)));

			if (CurrentWeapon)
			{
				FVector Displacement = FVector(0);
				switch (MovementState)
				{
				case EMovementState::Aim_State:
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::Walk_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Run_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				default:
					break;
				}

				CurrentWeapon->ShootEndLocation = ResultOfHit.Location + Displacement;
			}
		}
	}
}

bool AMyTDSGameCharacter::TrySwitchWeaponToIndexByKeyInput(int32 ToIndex)
{
	bool bIsSuccess = false;

	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
	{
		if (CurrentIndexWeapon != ToIndex && InventoryComponent)
		{
			int32 OldIndex = CurrentIndexWeapon;
			FAdditionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AdditionalWeaponInfo;

				if (CurrentWeapon->WeaponReloading)
				{
					CurrentWeapon->CancelReload();
				}

				bIsSuccess = InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
			}
		}
	}
	return bIsSuccess;
}

void AMyTDSGameCharacter::DropCurrentWeapon()
{
	if (InventoryComponent)
	{
		FDropItem ItemInfo;

		InventoryComponent->DropWeaponByIndex(CurrentIndexWeapon, ItemInfo);
	}
}

//������ ���������� ������� ������� � ������
void AMyTDSGameCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();

	if (myWeapon)
	{
		myWeapon->SetWeaponStateIsFiring(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("AMyTDSGameCharacter::AttackCharEvent - CurrentWeapon -NULL"));

	}
}

//
void AMyTDSGameCharacter::CharacterUpdate()
{
	if (bIsAlive && !bIStuned)
	{
		float ResultSpeed = 600.0f;
		switch (MovementState)
		{
		case EMovementState::Aim_State:
			ResultSpeed = MovementInfo.AimSpeed;
			break;
		case EMovementState::Walk_State:
			ResultSpeed = MovementInfo.WalkSpeed;
			break;
		case EMovementState::Run_State:
			ResultSpeed = MovementInfo.RunSpeed;
			break;
		default:
			break;
		}

		GetCharacterMovement()->MaxWalkSpeed = ResultSpeed;
}
}

//��������� � ��������� ������ ������� ��������� ������������ ��������� ���������
//����� �� ���� �������� �������� ��������
void AMyTDSGameCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	CharacterUpdate();

	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

//
AWeaponDefault* AMyTDSGameCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}


// InitWapon
void AMyTDSGameCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UMyTDSGameInstance* myGI = Cast<UMyTDSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));

				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("Weapon_Socket_R_Hand"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSettings = myWeaponInfo;
					//myWeapon->AdditionalWeaponInfo.Round = myWeaponInfo.MaxRound;

					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;

					CurrentIndexWeapon = NewCurrentIndexWeapon;

					myWeapon->OnWeaponRealoadStart.AddDynamic(this, &AMyTDSGameCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &AMyTDSGameCharacter::WeaponReloadEnd);

					myWeapon->OnWeaponFireStart.AddDynamic(this, &AMyTDSGameCharacter::WeaponFireStart);

					//�������������� ����������� ��� ������������ ������(���� ��������)
					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon -> CheckCanWeaponReload())
					{
						CurrentWeapon->initReload();
					}

					if (InventoryComponent)
					{
						InventoryComponent->OnWeaponAmmoAvailable.Broadcast(myWeapon->WeaponSettings.WeaponType);
					}
				}

			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}


}

//������� ����������� ������ ��� �������, ��� � ����� ������ ������� �����-���� ������ � ���� ��� ��� �� �������������
//������� ��������� ����������� �������� � �������� � ���� ��� ������ ������������� �� �������� ������� InitReload ������� �������� �����������
void AMyTDSGameCharacter::TryReloadWeapon()
{
	if (bIsAlive && CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSettings.MaxRound && CurrentWeapon -> CheckCanWeaponReload())
		{
			CurrentWeapon->initReload();
		}
	}
}

//
void AMyTDSGameCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

//
void AMyTDSGameCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent -> AmmoSlotChangeValue (CurrentWeapon -> WeaponSettings.WeaponType, AmmoTake);

		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);

		CurrentWeapon->CheckCanWeaponReload();
	}

	WeaponReloadEnd_BP(bIsSuccess);
}

void AMyTDSGameCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
		
	WeaponFireStart_BP(Anim);
} 


//
void AMyTDSGameCharacter::TrySwitchNextWeapon()
{
	if (bIsAlive)
	{
		if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
		{
			int32 OldIndex = CurrentIndexWeapon;

			FAdditionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AdditionalWeaponInfo;
				if (CurrentWeapon->WeaponReloading)
				{
					CurrentWeapon->CancelReload();
				}
			}

			if (InventoryComponent)
			{
				if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
				{

				}
			}
		}
	}
}

void AMyTDSGameCharacter::TrySwitchPreviousWeapon()
{
	if (bIsAlive)
	{
		if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
		{
			int8 OldIndex = CurrentIndexWeapon;
			FAdditionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AdditionalWeaponInfo;

				if (CurrentWeapon->WeaponReloading)
				{
					CurrentWeapon->CancelReload();
				}
			}

			if (InventoryComponent)
			{
				if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
				{

				}
			}
		}
	}
}

//������������� �����������
void AMyTDSGameCharacter::TryAbilityEnabled()
{
	if (AbilityEffect)
	{
		UMyTDSGame_StatusEffect* NewStatusEffect = NewObject<UMyTDSGame_StatusEffect>(this, AbilityEffect);

		TEnumAsByte<EPhysicalSurface> CharSurface = AMyTDSGameCharacter::GetMesh()->GetMaterial(0)->GetPhysicalMaterial()->SurfaceType;


		UTypes::AddStatusEffectBySurfaceType(this, NAME_None, AbilityEffect, CharSurface);

		/*
		if (NewStatusEffect)
		{
			NewStatusEffect->InitializeObject(this);
		}
		*/
	}
}

void AMyTDSGameCharacter::TryAbilityEnabled2()
{
	if (AbilityEffect2)
	{
		UMyTDSGame_StatusEffect* NewStatusEffect = NewObject<UMyTDSGame_StatusEffect>(this, AbilityEffect2);

		TEnumAsByte<EPhysicalSurface> CharSurface = AMyTDSGameCharacter::GetMesh()->GetMaterial(0)->GetPhysicalMaterial()->SurfaceType;


		UTypes::AddStatusEffectBySurfaceType(this, NAME_None, AbilityEffect2, CharSurface);

		/*
		if (NewStatusEffect)
		{
			NewStatusEffect->InitializeObject(this);
		}
		*/
	}
}

void AMyTDSGameCharacter::TryAbilityEnabled3()
{
	if (AbilityEffect3)
	{
		UMyTDSGame_StatusEffect* NewStatusEffect = NewObject<UMyTDSGame_StatusEffect>(this, AbilityEffect3);

		TEnumAsByte<EPhysicalSurface> CharSurface = AMyTDSGameCharacter::GetMesh()->GetMaterial(0)->GetPhysicalMaterial()->SurfaceType;


		UTypes::AddStatusEffectBySurfaceType(this, NAME_None, AbilityEffect3, CharSurface);

		/*
		if (NewStatusEffect)
		{
			NewStatusEffect->InitializeObject(this);
		}
		*/
	}
}

void AMyTDSGameCharacter::TryAbilityEnabled4()
{
	if (AbilityEffect4)
	{
		UMyTDSGame_StatusEffect* NewStatusEffect = NewObject<UMyTDSGame_StatusEffect>(this, AbilityEffect4);

		TEnumAsByte<EPhysicalSurface> CharSurface = AMyTDSGameCharacter::GetMesh()->GetMaterial(0)->GetPhysicalMaterial()->SurfaceType;


		UTypes::AddStatusEffectBySurfaceType(this, NAME_None, AbilityEffect4, CharSurface);

		/*
		if (NewStatusEffect)
		{
			NewStatusEffect->InitializeObject(this);
		}
		*/
	}
}

void AMyTDSGameCharacter::TryAbilityEnabled5()
{
	if (AbilityEffect5)
	{
		UMyTDSGame_StatusEffect* NewStatusEffect = NewObject<UMyTDSGame_StatusEffect>(this, AbilityEffect5);

		TEnumAsByte<EPhysicalSurface> CharSurface = AMyTDSGameCharacter::GetMesh()->GetMaterial(0)->GetPhysicalMaterial()->SurfaceType;


		UTypes::AddStatusEffectBySurfaceType(this, NAME_None, AbilityEffect5, CharSurface);

		/*
		if (NewStatusEffect)
		{
			NewStatusEffect->InitializeObject(this);
		}
		*/
	}
}


//��������� PhysicalSurface
EPhysicalSurface AMyTDSGameCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;

	if (CharacterHealthComponent)
	{
		if (CharacterHealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* MyMaterial = GetMesh()->GetMaterial(0);

				if (MyMaterial)
				{
					Result = MyMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}

		}
	}

	return Result;
}

//Status effects
TArray<UMyTDSGame_StatusEffect*> AMyTDSGameCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void AMyTDSGameCharacter::RemoveEffect(UMyTDSGame_StatusEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void AMyTDSGameCharacter::AddEffect(UMyTDSGame_StatusEffect* AddEffect)
{
	Effects.Add(AddEffect);
}

void AMyTDSGameCharacter::CharDead()
{
	bIsAlive = false;
	GetCharacterMovement()->bOrientRotationToMovement = false;

	if (GetController())
	{
		GetController() -> UnPossess();
	}

	UnPossessed();

	float TimeAnim = 0.0f;

	int32 rnd = FMath::RandHelper(DeathAnim.Num());

	if (DeathAnim.IsValidIndex(rnd) && DeathAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = DeathAnim[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeathAnim[rnd]);
	}

	//Timer ragdoll
	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer,this, &AMyTDSGameCharacter::EnableCharRagDoll, TimeAnim, false);

	CurrentCursor->SetVisibility(false);

	AttackCharEvent(false);

	CharDead_BP();
}

void  AMyTDSGameCharacter::CharDead_BP_Implementation()
{
	//BP
}

void AMyTDSGameCharacter::EnableCharRagDoll()
{

	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float AMyTDSGameCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	//Basic Damage
	float RecievedDamage = 0;
	RecievedDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (bIsAlive)
	{
		CharacterHealthComponent->ChangeHealthValue(RecievedDamage *-1);
	}


	//Status Effect
	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* MyProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (MyProjectile)
		{
			UTypes::AddStatusEffectBySurfaceType(this, NAME_None, MyProjectile->ProjectileSettings.StatusEffect, GetSurfaceType());
		}
	}

	return -RecievedDamage;
}

void AMyTDSGameCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{

}

void AMyTDSGameCharacter::WeaponReloadEnd_BP_Implementation(bool bISSuccess)
{
	//BP
}

void AMyTDSGameCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{

}

void AMyTDSGameCharacter::CharStunned()
{
	bIStuned = true;
}

void AMyTDSGameCharacter::StunStarted()
{
	if (bIStuned)
	{

		//������ ���� �� ����� Pawn �� MyTDSCharacter_BP_C_0 � � ��� �������� GetController
		//��������� ������� ���������� � ���������� ���� ACOntroller

		CharController = Cast<APawn>(this)->GetController();
		
		UnPossessed();
	}
}

void AMyTDSGameCharacter::StunEnded()
{
	bIStuned = false;

	if (CharController)
	{
		CharController->Possess(this);

	}
}
