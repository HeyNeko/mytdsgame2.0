// Fill out your copyright notice in the Description page of Project Settings.


#include "../Character/MyTDSGame_InventoryComponent.h"
#include "../Game/MyTDSGameInstance.h"

// Sets default values for this component's properties
UMyTDSGame_InventoryComponent::UMyTDSGame_InventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UMyTDSGame_InventoryComponent::BeginPlay()
{
	Super::BeginPlay();

}


// Called every frame
void UMyTDSGame_InventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

//����������� ������ � ��� �������� ������� ������ � ���������
bool UMyTDSGame_InventoryComponent:: SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward)
{
	bool bIsSuccess = false;
	int32 CorrectIndex = ChangeToIndex;

	//���������, ����� ������ �� ��� ������ ��� ������������ �������� �������
	//���� ��� ���, �� ����� ������� ������ ������ �  ������
	if (ChangeToIndex > WeaponSlots.Num()-1)
	{
		CorrectIndex = 0;
	}
	//���������, ����� ������ �� ��� ������ ��� ���������� �������� �������
	//���� ��� ���, �� ����� ������� ��������� ������ � ������
	else
	{
		if (ChangeToIndex < 0)
		{
			CorrectIndex = WeaponSlots.Num() - 1;
		}
	}

	//������ ����� ��������� ��� �������� ������ ������ ������
	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalWeaponInfo;
	int32 NewCurrentIndex = 0;
	
	if (WeaponSlots.IsValidIndex(CorrectIndex))
	{
		if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
		{
			if (WeaponSlots[CorrectIndex].AdditionalWeaponInfo.Round > 0)
			{
				//������������ �� ������� CorrectIndex ������ �������
				//������ � ����� �������� ���������� � ��� ������� �� ����� 0
				bIsSuccess = true;
			}
			//���� � ������� ������ ��� �������� � ������ �� ���� ���������
			//� ���� � ��� ������� ��� ����� ������
			else
			{
				UMyTDSGameInstance* myGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());

				if (myGI)
				{
					//�������� �������� AmmoSlots ��� ����� ������
					FWeaponInfo myInfo;
					myGI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, myInfo);

					//�������� �� ��������� � ���� ������� ��� ���������� ������
					//���� ������� � ��������� ���� �� ����������
					bool bIsFind = false;
					int8 j = 0;
					while (j < AmmoSlots.Num() && !bIsFind)
					{
						if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
						{
		
							bIsSuccess = true;
							bIsFind = true;

						}
						//���� � ��������� �������� ���� ���, �� ����� � ��� ������
						//� �������� ������ ������ �� �������(�������� ��������)
						else
						{
							UE_LOG(LogTemp, Error, TEXT("MyTDSGameInventoryComponent::SwitchWeaponToIndex - AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0"), 0);
						}
						j++;
					}
				}
			}
			//���� ������ ���������� � � ��� ��� � ��������� ���� ������� �� ������
			//������ �� ������ ������ ������  ������
			if (bIsSuccess)
			{
				NewCurrentIndex = CorrectIndex;
				NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
				NewAdditionalWeaponInfo = WeaponSlots[CorrectIndex].AdditionalWeaponInfo;
			}
		}
		//���� ������ � ����� ������ ���
		else
		{
			UE_LOG(LogTemp, Error, TEXT("MyTDSGameInventoryComponent::SwitchWeaponToIndex - WeaponSlots[CorrectIndex].NameItem - Is None"), CorrectIndex);
		}
	}
	//���� �������� ������
	else
	{
		UE_LOG(LogTemp, Error, TEXT("MyTDSGameInventoryComponent::SwitchWeaponToIndex - CorrectIndex %d is incorrect"), CorrectIndex);
	}

	//���� �� �����-�� ������� �� ���� ������� ����� ������ � ��������� � ������ ��� ���������
	//�������� ����� ���������� ������ ������� ����� ���� �� ���� ���������
	if (!bIsSuccess)
	{
		//���������, �������� � ������ ������� ��� ��������� ����� ���� �������
		//�� ������ ������� ���� �������

			int8 Iteration = 0;
			int8 SecondIteration = 0;
			int8 tmpIndex = 0;

			while (Iteration < WeaponSlots.Num() && !bIsSuccess)
			{
				Iteration++;

				if (bIsForward)
				{
					tmpIndex = ChangeToIndex + Iteration;
				}
				else
				{
					SecondIteration = WeaponSlots.Num() - 1;
					tmpIndex = ChangeToIndex - Iteration;
				}


				if (WeaponSlots.IsValidIndex(tmpIndex))
				{
					if (!WeaponSlots[tmpIndex].NameItem.IsNone())
					{
						if (WeaponSlots[tmpIndex].AdditionalWeaponInfo.Round > 0)
						{
							//��������� ������ ���������� � ����� ���� ������� ������ ���� ���
							bIsSuccess = true;
							NewIdWeapon = WeaponSlots[ChangeToIndex].NameItem;
							NewAdditionalWeaponInfo = WeaponSlots[ChangeToIndex].AdditionalWeaponInfo;
							NewCurrentIndex = tmpIndex;
						}
						//���� ������ ��� ��� � ���� 0 ��������, ���� � ��������� ����������� �������� ��� ������ ������
						else
						{
							FWeaponInfo myInfo;
							UMyTDSGameInstance* myGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());

							myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

							bool bIsFind = false;
							int8 j = 0;
							while (j < AmmoSlots.Num() && !bIsFind)
							{
								if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
								{
									//���� ������� ������������� ������ � �� ������ 0
									bIsSuccess = true;
									NewIdWeapon = WeaponSlots[j].NameItem;
									NewAdditionalWeaponInfo = WeaponSlots[j].AdditionalWeaponInfo;
									NewCurrentIndex = tmpIndex;
									bIsFind = true;
								}
								j++;
							}
						}
					}
				}
				//���� �������� ChangeToIndex + Iteration �� ���� ����������� ������ � ���������
				//�������� ������ �������� ������� � 0-�� �������
				else
				{
					if (OldIndex != SecondIteration)
					{
						if (WeaponSlots.IsValidIndex(SecondIteration))
						{
							if (!WeaponSlots[SecondIteration].NameItem.IsNone())
							{
								if (WeaponSlots[SecondIteration].AdditionalWeaponInfo.Round > 0)
								{
									//���� ������ ���������� � � ���� ���� ������� � ������
									bIsSuccess = true;
									NewIdWeapon = WeaponSlots[SecondIteration].NameItem;
									NewAdditionalWeaponInfo = WeaponSlots[SecondIteration].AdditionalWeaponInfo;
									NewCurrentIndex = SecondIteration;
								}
								//���� �������� ���, �� ��������� ������� �������� � ���������
								else
								{
									FWeaponInfo myInfo;
									UMyTDSGameInstance* myGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponSlots[SecondIteration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;

									while (j < AmmoSlots.Num() && !bIsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
										{
											//���� � ��������� ���� AmmoSlots ��� ���������������� ������ � � ��� ���� �������
											bIsSuccess = true;
											NewIdWeapon = WeaponSlots[SecondIteration].NameItem;
											NewAdditionalWeaponInfo = WeaponSlots[SecondIteration].AdditionalWeaponInfo;
											NewCurrentIndex = SecondIteration;
											bIsFind = true;
										}
										j++;
									}
								}
							}
						}
					}
					//���� � ������ ������ ��� �� ����� ������, ������ ��������
					//������� ��� � ���� �� ������ � �������� �� ������ ������������
					//������ �������� �������� ��� ����
					else
					{
						if (WeaponSlots.IsValidIndex(SecondIteration))
						{
							if (!WeaponSlots[SecondIteration].NameItem.IsNone())
							{
								if (WeaponSlots[SecondIteration].AdditionalWeaponInfo.Round > 0)
								{
									//��� �� �� ����� ������ � � ���� �������� �������, ������ ��������� �� ����
								}
								//��������� �������� �� ������� � ���������
								else
								{
									FWeaponInfo myInfo;
									UMyTDSGameInstance* myGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponSlots[SecondIteration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlots.Num() && !bIsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
										{
											if (AmmoSlots[j].Cout > 0)
											{
												//� ��������� ��� ���� �������, ������ �� ���� ������
											}
											else
											{
												//�������� ��� �� ��� ������ ������, ���������������� �������� ������ - �������� � ������������ ���������
												UE_LOG(LogTemp, Error, TEXT("UMyTDSGameInventoryComponent::SwitchWeaponToIndex - Init Pistol - NEED"))
											}
										}
										j++;
									}
								}
							}
						}
					}
					if (bIsForward)
					{
						SecondIteration++;
					}
					else
					{
						SecondIteration--;
					}
				}
			}
	}
		/*
		//��� ������ ���� �� ������������� �� �� �������� ������
		// � �� ����������
		//���������, �������� � ������ ������� � ���������� ���� �������
		else
		{
			int8 Iteration = 0;
			int8 SecondIteration = WeaponSlots.Num() - 1;

			while (Iteration < WeaponSlots.Num() && !bIsSuccess)
			{
				Iteration++;
				int8 tmpIndex = ChangeToIndex - Iteration;

				if (WeaponSlots.IsValidIndex(tmpIndex))
				{
					if (!WeaponSlots[tmpIndex].NameItem.IsNone())
					{
						if (WeaponSlots[tmpIndex].AdditionalWeaponInfo.Round > 0)
						{
							//���������� ������ ���������� � � ���� ���� �������
							bIsSuccess = true;
							NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
							NewAdditionalWeaponInfo = WeaponSlots[tmpIndex].AdditionalWeaponInfo;
							NewCurrentIndex = tmpIndex;
						}
						else
						{
							FWeaponInfo myInfo;
							UMyTDSGameInstance* myGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());

							myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

							bool bIsFind = false;
							int8 j = 0;
							while (j < AmmoSlots.Num() && !bIsFind)
							{
								if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
								{
									//� ��������� ���� ������� ��� ����� ������
									bIsSuccess = true;
									NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
									NewAdditionalWeaponInfo = WeaponSlots[tmpIndex].AdditionalWeaponInfo;
									NewCurrentIndex = tmpIndex;
									bIsFind = true;
								}
								j++;
							}
						}
					}
				}
				else
				{
					//�������� ����� �� ����� ������
					if (OldIndex != SecondIteration)
					{
						if (WeaponSlots.IsValidIndex(SecondIteration))
						{
							if (!WeaponSlots[SecondIteration].NameItem.IsNone())
							{
								if (WeaponSlots[SecondIteration].AdditionalWeaponInfo.Round > 0)
								{
									//��������� ��������� � ����� ����� ������ ������������
									//� � ���� �������� �������
									bIsSuccess = true;
									NewIdWeapon = WeaponSlots[SecondIteration].NameItem;
									NewAdditionalWeaponInfo = WeaponSlots[SecondIteration].AdditionalWeaponInfo;
									NewCurrentIndex = SecondIteration;
								}
								//������� ��� � ������
								//���� � ���������
								else
								{
									FWeaponInfo myInfo;
									UMyTDSGameInstance* myGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponSlots[SecondIteration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlots.Num() && !bIsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
										{
											//���� � �������� ���� ������� ��� ����� ������ �� ���� ��
											bIsSuccess = true;
											NewIdWeapon = WeaponSlots[SecondIteration].NameItem;
											NewAdditionalWeaponInfo = WeaponSlots[SecondIteration].AdditionalWeaponInfo;
											NewCurrentIndex = SecondIteration;
											bIsFind = true;
										}
										j++;
									}
								}
							}
						}
					}
					//������ �������� ������ ����� SecondIteration
					else
					{
					//���� ��������� � ���� �� ������ ������� ����
						if (WeaponSlots.IsValidIndex(SecondIteration))
						{
							if (!WeaponSlots[SecondIteration].NameItem.IsNone())
							{
								if (WeaponSlots[SecondIteration].AdditionalWeaponInfo.Round > 0)
								{
									//� ������ ��� ���� ������� ������ ������ �� ����
								}
								else
								{
									FWeaponInfo myInfo;
									UMyTDSGameInstance* myGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());
		
									myGI->GetWeaponInfoByName(WeaponSlots[SecondIteration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
	
									while (j < AmmoSlots.Num() && !bIsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
										{
											if (AmmoSlots[j].Cout > 0)
											{
												//� ��������� ��� ����� ������ ��� ��� �������
											}
											else
											{
												//��� ������ � ���������, ������������� ��������� � ������������ ���������
												UE_LOG(LogTemp, Error, TEXT("UMyTDSGameInventoryComponent::SwitchWeaponToIndex - Init Pistol - NEED"))
											}
										}
										j++;
									}
								}
							}
						}
					}
					SecondIteration--;
				}
			}
		}

	}
	*/
	//���� ����� �������� ����, ��� ������������ �� ����� ������ � ������� ChageToIndex ��������
	//������ ��� ���������� � � ���� ���� �������� ������� � ������ ���� ���� � ���������
	//�������� ������� ������� �������������� ������ ������ � ���������
	//� ����� ������ Broadcast ����� �������� ��������� ����� ������ � ��� ����� ������
	if (bIsSuccess)
	{
		SetAdditionalInfoWeapon(OldIndex, OldInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalWeaponInfo, NewCurrentIndex);
	}

	return bIsSuccess;
}

bool UMyTDSGame_InventoryComponent::SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviousIndex, FAdditionalWeaponInfo PreviousWeaponInfo)
{
	bool bIsSuccess = false;
	FName ToSwitchIdWeapon;
	FAdditionalWeaponInfo ToSwitchAdditionalInfo;

	ToSwitchIdWeapon = GetWeaponNameBySlotIndex(IndexWeaponToChange);
	ToSwitchAdditionalInfo = GetAdditionalWeaponInfo(IndexWeaponToChange);

	if (!ToSwitchIdWeapon.IsNone())
	{
		SetAdditionalInfoWeapon(PreviousIndex, PreviousWeaponInfo);
		OnSwitchWeapon.Broadcast(ToSwitchIdWeapon, ToSwitchAdditionalInfo, IndexWeaponToChange);

		//Check ammo slot for event to player
		EWeaponType ToSwitchWeaponType;

		if (GetWeaponTypeByNameWeapon(ToSwitchIdWeapon, ToSwitchWeaponType))
		{
			int8 AvailableAmmoForWeapon = -1;
			if (CheckAmmoForWeapon(ToSwitchWeaponType, AvailableAmmoForWeapon))
			{

			}
		}
		bIsSuccess = true;
	}
	return bIsSuccess;
}

FAdditionalWeaponInfo UMyTDSGame_InventoryComponent::GetAdditionalWeaponInfo(int32 IndexWeapon)
{
	FAdditionalWeaponInfo result;
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;

		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (/*WeaponSlots[i].IndexSlot*/i == IndexWeapon)
			{
				result = WeaponSlots[i].AdditionalWeaponInfo;
				bIsFind = true;
			}
			i++;
		}
		UE_LOG(LogTemp, Warning, TEXT("MyTDSGameInventoryComponent::SetAdditionalWeaponInfo - Weapon with index %d not found"), IndexWeapon);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("MyTDSGameInventoryComponent::SetAdditionalWeaponInfo - Weapon index %d is incorrect"), IndexWeapon);
	}

	return result;
}

int32 UMyTDSGame_InventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFind = false;

	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			bIsFind = true;
			result = i/*WeaponSlots[i].IndexSlot*/;
		}
		i++;
	}
	return result;
}

FName UMyTDSGame_InventoryComponent::GetWeaponNameBySlotIndex(int32 IndexSlot)
{
	FName result;
	
	if (WeaponSlots.IsValidIndex(IndexSlot))
	{
		result = WeaponSlots[IndexSlot].NameItem;
	}

	return result;
}

bool UMyTDSGame_InventoryComponent::GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType &WeaponType)
{
	bool bIsFind = false;
	FWeaponInfo OutInfo;
	WeaponType = EWeaponType::Pistol;
	UMyTDSGameInstance* MyGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());

	if (MyGI)
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			MyGI->GetWeaponInfoByName(WeaponSlots[IndexSlot].NameItem, OutInfo);
			WeaponType = OutInfo.WeaponType;
			bIsFind = true;
		}
	}
	return bIsFind;
}

bool UMyTDSGame_InventoryComponent::GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType& WeaponType)
{
	bool bIsFind = false;
	FWeaponInfo OutInfo;
	WeaponType = EWeaponType::Pistol;
	UMyTDSGameInstance* MyGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());

	if (MyGI)
	{
		MyGI->GetWeaponInfoByName(IdWeaponName, OutInfo);
		WeaponType = OutInfo.WeaponType;
		bIsFind = true;
	}
	return bIsFind;
}

void UMyTDSGame_InventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;

		while (i<WeaponSlots.Num() && !bIsFind)
		{
			if (/*WeaponSlots[i].IndexSlot */ i == IndexWeapon)
			{ 
				WeaponSlots[i].AdditionalWeaponInfo = NewInfo;
				bIsFind = true;

				OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!bIsFind)
		{
			UE_LOG(LogTemp, Warning, TEXT("MyTDSGameInventoryComponent::SetAdditionalWeaponInfo - Weapon with index %d not found"), IndexWeapon);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("MyTDSGameInventoryComponent::SetAdditionalWeaponInfo - Weapon index %d is incorrect"), IndexWeapon);
	}
}

void UMyTDSGame_InventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo)
{
	bool bIsFind = false;

	int8 i = 0;

	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			AmmoSlots[i].Cout = AmmoSlots[i].Cout + CoutChangeAmmo;

			if (AmmoSlots[i].Cout > AmmoSlots[i].MaxCout)
			{
				AmmoSlots[i].Cout = AmmoSlots[i].MaxCout;
			}

			//To cout ammochange
			OnAmmoChange.Broadcast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);

			bIsFind = true;
		}

		i++;
	}
}

bool UMyTDSGame_InventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int8 &AvailableAmmoForWeapon)
{
	AvailableAmmoForWeapon = 0;
	bool bIsFind = false;
	int8 i = 0;

	while (i<AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			bIsFind = true;
			AvailableAmmoForWeapon = AmmoSlots[i].Cout;

			if (AmmoSlots[i].Cout > 0)
			{
				return true;
			}
		}

		i++;
	}

	if (AvailableAmmoForWeapon <= 0)
	{
		OnWeaponAmmoEmpty.Broadcast(TypeWeapon);//Broadcast to widget for visuals
	}
	else
	{
		OnWeaponAmmoAvailable.Broadcast(TypeWeapon);//Broadcast to widget for visuals
	}
	

	return false;
}

bool UMyTDSGame_InventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool result = false;
	int8 i = 0;

	while (i < AmmoSlots.Num() && !result)
	{
		if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Cout < AmmoSlots[i].MaxCout)
		{
			result = true;
		}
		i++;
	}
	return result;
}

//��������� ��� ���� ������ ���� ������
//���� � ���, ��� IsNone ������ true �� ������ ���� ���� ���������� ���
//�� � ���� ���� ���������� �� � ��� ������ ������
bool UMyTDSGame_InventoryComponent::CheckCanTakeWeapon(int32 &FreeSlot)
{
	bool bIsFreeSlot = false;
	int8 i = 0;

	while (i < WeaponSlots.Num() && !bIsFreeSlot)
	{
		if (WeaponSlots[i].NameItem.IsNone())
		{
			bIsFreeSlot = true;
			FreeSlot = i;
		}
		i++;
	}
	return bIsFreeSlot;
}

bool UMyTDSGame_InventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItem& DropItemInfo)
{
	bool result = false;

	if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo))
	{
		WeaponSlots[IndexSlot] = NewWeapon;

		SwitchWeaponToIndex(CurrentIndexWeaponChar,-1,NewWeapon.AdditionalWeaponInfo, true);

		OnUpdateWeaponSlots.Broadcast(IndexSlot,NewWeapon);

		result = true;

	}

	return result;
}

bool UMyTDSGame_InventoryComponent::TryGetWeaponToInventory(FWeaponSlot NewWeapon)
{
	int32 IndexSlot = -1;
	if (CheckCanTakeWeapon(IndexSlot))
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			WeaponSlots[IndexSlot] = NewWeapon;

			OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);

			return true;
		}
	}
	return false;
}

void UMyTDSGame_InventoryComponent::DropWeaponByIndex(int32 ByIndex, FDropItem  &DropItemInfo)
{
	//������ ������ ����
	FWeaponSlot EmptyWeaponSlot;

	bool bIsCanDrop = false;
	int8 i = 0;
	int8 AvailableWeaponNum = 0;

	//��������� ��� � ��������� ���� ������� ���� ������, ����� ��������� ����� ������

	while (i < WeaponSlots.Num() && !bIsCanDrop)
	{
		if (!WeaponSlots[i].NameItem.IsNone())
		{
			AvailableWeaponNum++;
			if (AvailableWeaponNum >= 1)
			{
				bIsCanDrop = true;
			}
			
		}
		i++;
	}

	//���� ������ �� ������(�� ��������) � � ������� WeaponSlots � �������� ��������� � ������ �� ����� 
	//� ���� ������� GetDropInfoFromInventory ����� ���� ��������� � ������������ �������, ��
	if (bIsCanDrop && WeaponSlots.IsValidIndex(ByIndex) && GetDropItemInfoFromInventory(ByIndex, DropItemInfo))
	{
		//��� ������� ���������� � DropItemInfo WeaponStaticMEsh/WeaponInfo(Name-��� �������������� ������, Round ������� � ��� ����)
		GetDropItemInfoFromInventory(ByIndex, DropItemInfo);

		//switch weapon to valid slot weapon from start of array
		bool bIsFindWeapon = false;
		int8 j = 0;

		//������� ������ �� ������ ���� � ������� � ������������� �� ����
		while(j<WeaponSlots.Num() && !bIsFindWeapon)
		{
			if (!WeaponSlots[j].NameItem.IsNone())
			{
				OnSwitchWeapon.Broadcast(WeaponSlots[j].NameItem, WeaponSlots[j].AdditionalWeaponInfo, j);
				bIsFindWeapon = true;
			}
			j++;
		}

		//���������� �  ������ �� ������� WeaponSLots ��� ������� ���� ������ ������ EmptyWeaponSLots, ������� ����������
		WeaponSlots[ByIndex] = EmptyWeaponSlot;

		//���� ��������� ��������� ���������� ����� ����� ���������� - IGameActor �� �������� ������� DropWeaponToWorld
		//� ������� ������� ���������� � ������������� ������
		if (GetOwner()->GetClass()->ImplementsInterface(UMyTDSGame_IGameActor::StaticClass()))
		{
			IMyTDSGame_IGameActor::Execute_DropWeaponToWorld(GetOwner(), DropItemInfo);
		}

		//�������� �������� UpdateWeaponSlot � ������� ����� ����� � ������ ���������� � WeaponSlots � �������� �������������� ������
		OnUpdateWeaponSlots.Broadcast(ByIndex, EmptyWeaponSlot);
	}
}

bool UMyTDSGame_InventoryComponent::GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem &DropItemInfo)
{
	bool result = false;

	FName DropItemName = GetWeaponNameBySlotIndex(IndexSlot);

	UMyTDSGameInstance* myGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());

	if (myGI)
	{
		result = myGI->GetWeaponInfoByWeaponName(DropItemName, DropItemInfo);

		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			DropItemInfo.WeaponInfo.AdditionalWeaponInfo = WeaponSlots[IndexSlot].AdditionalWeaponInfo;
		}
		
	}

	return result;
}

TArray<FWeaponSlot>  UMyTDSGame_InventoryComponent::GetWeaponSLots()
{
	return WeaponSlots;
}

TArray<FAmmoSlot>  UMyTDSGame_InventoryComponent::GetAmmoSLots()
{
	return AmmoSlots;
}

void  UMyTDSGame_InventoryComponent::InitInventory(TArray<FWeaponSlot> NewWeaponSlotsInfo, TArray<FAmmoSlot> NewAmmoSlotsInfo)
{
	WeaponSlots = NewWeaponSlotsInfo;
	AmmoSlots = NewAmmoSlotsInfo;

	// Find init weaponSLots and First init Weapon
	for (int8 i = 0; i < WeaponSlots.Num(); i++)
	{
		UMyTDSGameInstance* MyGI = Cast<UMyTDSGameInstance>(GetWorld()->GetGameInstance());
		if (MyGI)
		{
			if (!WeaponSlots[i].NameItem.IsNone())
			{
				//FWeaponInfo Info;

				//if (MyGI->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info))
				//{
					//WeaponSlots[i].AdditionalWeaponInfo.Round = Info.MaxRound;
				//}
			}
		}
	}

	MaxWeaponSlots = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].NameItem.IsNone())
		{
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalWeaponInfo, 0);
		}
	}
}