 // Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../FunctionLibrary/Types.h"
#include "../Game/WeaponDefault.h"
#include "../Character/MyTDSGame_InventoryComponent.h"
#include "../Character/MyMyTDSGame_CharHealthComponent.h"
#include "../Game/MyTDSGamePlayerController.h"
#include "../Interface/MyTDSGame_IGameActor.h" 
#include "../Game/MyTDSGame_StatusEffect.h"

#include "MyTDSGameCharacter.generated.h"


UCLASS(Blueprintable)
class AMyTDSGameCharacter : public ACharacter, public IMyTDSGame_IGameActor//����� �������� ��������� ����� � ���������� ������ ��������� public I(��� ����������) 
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	AMyTDSGameCharacter();

	FTimerHandle TimerHandle_RagDollTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UMyTDSGame_InventoryComponent* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UMyMyTDSGame_CharHealthComponent* CharacterHealthComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;

public:
	//Flags
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flags")
		bool bIsAlive = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flags")
		bool bIStuned = false;

	//Death Anim Montage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Death Montage")
		TArray<UAnimMontage*> DeathAnim;

	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
	//
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Cursor")
		UDecalComponent* CurrentCursor = nullptr;

	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Walk_State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VisualFX")
		FParticleSysParam Shield;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VisualFX")
		FParticleSysParam ShieldRecieveDamage;

	//Inputs
	UFUNCTION()
		void InputAxisX(float value);

	UFUNCTION()
		void InputAxisY(float value);

	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();

	//Effects
	TArray<UMyTDSGame_StatusEffect*> Effects;

	//Weapon
	AWeaponDefault* CurrentWeapon = nullptr;

	//for demo
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
	FName InitWeaponName;

	//Tick Function
	UFUNCTION()
		void MovementTick(float DeltaTime);

	//Functions

	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
		void ChangeMovementState(EMovementState NewMovementState);

	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AtSprintState = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool Reloading = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UMyTDSGame_StatusEffect> AbilityEffect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability2")
		TSubclassOf<UMyTDSGame_StatusEffect> AbilityEffect2;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability3")
		TSubclassOf<UMyTDSGame_StatusEffect> AbilityEffect3;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability4")
		TSubclassOf<UMyTDSGame_StatusEffect> AbilityEffect4;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability5")
		TSubclassOf<UMyTDSGame_StatusEffect> AbilityEffect5;

	//
	UFUNCTION()
		void DropCurrentWeapon();

	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);

	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();

	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();

	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);

	//Reload Delegates
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);

	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);

	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);

	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);

	UFUNCTION()
		void WeaponFireStart (UAnimMontage* Anim);

	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP (UAnimMontage* Anim);

	UFUNCTION()
		bool TrySwitchWeaponToIndexByKeyInput(int32 ToIndex);

	//UFUNCTION(BlueprintCallable)
		//UDecalComponent* GetCursorToWorld();

	//Inventory Functions
	void TrySwitchNextWeapon();

	void TrySwitchPreviousWeapon();

	//Ability Functions
	void TryAbilityEnabled();
	void TryAbilityEnabled2();
	void TryAbilityEnabled3();
	void TryAbilityEnabled4();
	void TryAbilityEnabled5();

	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput(Id);
	}

	UPROPERTY(BLueprintReadOnly, EditDefaultsOnly)
	int32 CurrentIndexWeapon = 0;

	//Interface
	EPhysicalSurface GetSurfaceType() override;

	TArray<UMyTDSGame_StatusEffect*> GetAllCurrentEffects() override;

	void RemoveEffect(UMyTDSGame_StatusEffect* RemoveEffect) override;

	void AddEffect(UMyTDSGame_StatusEffect* AddEffect) override;
	//End Interface

	UFUNCTION()
	void CharDead();

	void EnableCharRagDoll();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION()
		void CharStunned();

	UFUNCTION()
		void StunStarted();

	UFUNCTION()
		void StunEnded();

	UPROPERTY()
		AController* CharController = nullptr;

	UFUNCTION(BlueprintNativeEvent)
		void CharDead_BP();
};

