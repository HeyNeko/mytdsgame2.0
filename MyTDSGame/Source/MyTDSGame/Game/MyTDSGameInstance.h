// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "../FunctionLibrary/Types.h"
#include "Engine/DataTable.h"
#include "../Game/WeaponDefault.h"
#include "MyTDSGameInstance.generated.h"

/**
 * 
 */

UCLASS()
class MYTDSGAME_API UMyTDSGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
		UDataTable* WeaponInfoTable = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
		UDataTable* DropItemInfoTable = nullptr;

	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);

	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByWeaponName(FName NameItem, FDropItem& OutInfo);

	UFUNCTION(BlueprintCallable)
		bool GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo);
};
