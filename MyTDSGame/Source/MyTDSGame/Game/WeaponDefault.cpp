// Fill out your copyright notice in the Description page of Project Settings.


#include "../Game/WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "../Character/MyTDSGame_InventoryComponent.h"

/*int32 DebugWeaponShow = 0;
FAutoConsoleVariableRef CVarWeaponShow(
	TEXT("TPS.DebugWeapon"),
	DebugWeaponShow,
	TEXT("Draw Debug for Weapon"),
	ECVF_Cheat);
*/
// Sets default values
AWeaponDefault::AWeaponDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Creating SceneComponent
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	//Setting RootComponent
	RootComponent = SceneComponent;

	//Creating Skeletal Mesh
	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh Weapon"));

	//Adjusting default settings of created Skeletal Mesh
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName("NoCollision");
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	//Creating Static Mesh
	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh Weapon"));

	//Adjusting default settings of created Static Mesh
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName("NoColision");
	StaticMeshWeapon->SetupAttachment(RootComponent);

	//Creating Arrow Component
	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Shooting Direction Arrow"));

	//Adjusting defaulst settings of Arrow Component
	ShootLocation->SetupAttachment(RootComponent);

	WeaponShells = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle Emitter BulletShell"));
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();

}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
}

//
void AWeaponDefault::FireTick(float DeltaTime)
{
	if (WeaponIsFiring && GetWeaponRound() > 0 && !WeaponReloading)
	{
		if (FireTimer < 0.f)
		{
			Fire();
		}
		else
		{
			FireTimer = FireTimer - DeltaTime;
		}
	}
}

//
void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer = ReloadTimer - DeltaTime;
		}
	}
}

//
void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponIsFiring)
		{
			if (ShouldReduceDispersion)
			{
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			}
			else
			{
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
			}
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	if (ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

// �������������� ������
void AWeaponDefault::WeaponInit()
{
	//���� ������ SkeletalMeshWEapon ���������� � ��� ���� � ��� ��� SkeletalMesh(������ null) �� �� �����������
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}
	//����� �� ������ ��� StaticMeshWeapon
	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent(true);
	}

	UpdateStateWeapon(EMovementState::Walk_State);
}

//
void AWeaponDefault::SetWeaponStateIsFiring(bool bIsFire)
{	//���� �� ������ ������� CheckWeaponCanFire �� ����, �� WeaponIsFiring ��������� ���������� ��������
	if (CheckWeaponCanFire())
	{
		WeaponIsFiring = bIsFire;
	}
	else
	{
		WeaponIsFiring = false;
		FireTimer = 0.01f; // ?
	}

}

//��� �������� ������ ����� ����� ��������
bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

//
FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSettings.ProjectileSettings;
}

//�������
void AWeaponDefault::Fire()
{
	UAnimMontage* AnimFireStart = nullptr;
	
	if (WeaponIsFiring)
	{
		AnimFireStart = WeaponSettings.AnimCharFire;
	}

	//����������� ���������� float FireTime �������� ��������� � SRUCT Weapon Settings � ����� RateOfFire
	FireTimer = WeaponSettings.RateOfFire;
	AdditionalWeaponInfo.Round = AdditionalWeaponInfo.Round - 1;
	ChangeDispersionByShot();

	OnWeaponFireStart.Broadcast(AnimFireStart);

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSettings.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSettings.EffectFireWeapon, ShootLocation->GetComponentTransform());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSettings.ShellBullets, ShootLocation->GetComponentTransform());

	int8 NumberProjectile = GetNumberProjectileByShot();

	if (ShootLocation)
	{
		//������ ���������� ���� FVector � ��������� ProjectileSpawnLocation � ���������� � ��
		//������� ArrowComponent(ShootLocation)
		FVector ProjectileSpawnLocation = ShootLocation->GetComponentLocation();

		//������ ���������� ���� FRotator � ��������� ProjectileSpawnRoation � ���������� � ��
		//�������� ������� ArrowComponent(ShootLocation)
		FRotator ProjectileSpawnRotation = ShootLocation->GetComponentRotation();

		//������ ������������� STRUCT FProjectileInfo � ��������� ProjectileInfo

		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector Endlocation;
		for (int8 i = 0; i < NumberProjectile; i++)// Projectiles for shotgun
		{
			Endlocation = GetFireEndLocation();

			FVector Dir = Endlocation - ProjectileSpawnLocation;

			Dir.Normalize();

			FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
			ProjectileSpawnRotation = myMatrix.Rotator();

			//���� Projectile ������(����������)
			if (ProjectileInfo.Projectile)
			{

				//������ ������� ���������� ������
				FActorSpawnParameters SpawnParams;

				//������������ ��������� ������ Projectile
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				//������� ������� Projectile'� AProjectileDefault, ������� � ��� Location � Rotation ArrowComponent(ShootLocation)
				//� ������� ������ ����� Projectile ������ ��������� � ��������� �� ArrowComponent 
				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &ProjectileSpawnLocation, &ProjectileSpawnRotation, SpawnParams));

				if (myProjectile)
				{
					myProjectile->InitProjectile(WeaponSettings.ProjectileSettings);
				}
			}
			else
			{
				FHitResult Hit;
				//������� ������� �������� LineTraceSingle � ApplyPointDamage 
				TraceCheck();
				TArray<AActor*> Actors;
				EDrawDebugTrace::Type DebugTrace;
				DebugTrace = EDrawDebugTrace::None;

				UKismetSystemLibrary::LineTraceSingle(GetWorld(), ProjectileSpawnLocation, Endlocation * WeaponSettings.DistanceTrace,
					ETraceTypeQuery::TraceTypeQuery4, false, Actors, DebugTrace, Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);


				EPhysicalSurface MySurfaceType = UGameplayStatics::GetSurfaceType(Hit);



				UTypes::AddStatusEffectBySurfaceType(Hit.GetActor(),Hit.BoneName, ProjectileInfo.StatusEffect, MySurfaceType);

			}


		}


	}
	UMyTDSGame_InventoryComponent* MyInventoryComponent = Cast<UMyTDSGame_InventoryComponent>(GetOwner()->
															GetComponentByClass(UMyTDSGame_InventoryComponent::StaticClass()));
	if(MyInventoryComponent)
	{
		if (GetWeaponRound() <= 0)
		{
			MyInventoryComponent->OnWeaponNotHaveRound.Broadcast(WeaponSettings.WeaponType);
		}
		else
		{
			MyInventoryComponent->OnWeaponHaveRound.Broadcast(WeaponSettings.WeaponType);
		}
	}
	/*if (GetWeaponRound() <= 0 && !WeaponReloading)
	{
		if (CheckCanWeaponReload())
		{
			initReload();
		}
	}
	*/
}

//
void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:

		CurrentDispersionMax = WeaponSettings.DispersionWeapon.AimingDispersion_AimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.AimingDispersion_AimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.AimingDispersion_AimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.AimingDispersion_DispersionReduction;
		break;

	case EMovementState::Walk_State:
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.WalkDispersion_AimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.WalkDispersion_AimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.WalkDispersion_AimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.WalkDispersion_DispersionReduction;
		break;

		/*case EMovementState::Run_State:
			CurrentDispersionMax = WeaponSettings.DispersionWeapon.RunDispersion_AimMax;
			CurrentDispersionMin = WeaponSettings.DispersionWeapon.RunDispersion_AimMin;
			CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.RunDispersion_AimRecoil;
			CurrentDispersionReduction = WeaponSettings.DispersionWeapon.RunDispersion_DispersionReduction;
			break;
		*/
	case EMovementState::Run_State:
		BlockFire = true;
		SetWeaponStateIsFiring(false);
		break;
	default:
		break;
	}
}

//
void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

//
float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

//
FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

//
FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);

	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (ShowDebug)
		{
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSettings.DistanceTrace, GetCurrentDispersion() * PI / 180.0f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
		}
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
		{
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSettings.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
		}
	}

	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);

		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);

		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

		//DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}

	return EndLocation;
}

//
int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSettings.NumberProjectileByShot;
}

//
void AWeaponDefault::ChangeDispersion()
{

}

//
int32 AWeaponDefault::GetWeaponRound()
{
	return AdditionalWeaponInfo.Round;
}

//
void AWeaponDefault::initReload()
{
	WeaponReloading = true;

	ReloadTimer = WeaponSettings.ReloadTime;

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSettings.MagazineDrop, ShootLocation->GetComponentTransform());

	if (WeaponSettings.AnimCharReload)
	{
		OnWeaponRealoadStart.Broadcast(WeaponSettings.AnimCharReload);
	}

}

//
void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;

	int8 AvailableAmmoFromInventory = GetAvailableAmmoForReload();

	int8 AmmoNeedTakeFromInv;

	int8 NeedToReload = WeaponSettings.MaxRound - AdditionalWeaponInfo.Round;


	if (NeedToReload > AvailableAmmoFromInventory)
	{
		AdditionalWeaponInfo.Round += AvailableAmmoFromInventory;
		AmmoNeedTakeFromInv = AvailableAmmoFromInventory;
	}
	else
	{
		AdditionalWeaponInfo.Round = AdditionalWeaponInfo.Round + NeedToReload;
		AmmoNeedTakeFromInv = NeedToReload;
	}

	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv);

}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon -> GetAnimInstance())
	{
		SkeletalMeshWeapon -> GetAnimInstance() -> StopAllMontages(0.15f);
	}

	OnWeaponReloadEnd.Broadcast(false, 0);

}

//��������� ���� �� � ������� ������ �������� � ���� �� � ����� ��������� ���������
//�� ������� ���� � ������ ��������� ���, �� � ���������� ��� �� � ����, ����� ������������������� ������
bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = true;

	if (GetOwner())
	{
		UE_LOG(LogTemp, Error, TEXT("AWeaponDefault::CheckCanWeaponReload - OwnerName - %s"), *GetOwner()->GetName());

		UMyTDSGame_InventoryComponent* MyInventoryComponent = Cast<UMyTDSGame_InventoryComponent> (GetOwner()->GetComponentByClass(UMyTDSGame_InventoryComponent::StaticClass()));

		if (MyInventoryComponent)
		{
			int8 AvailableAmmoForWeapon;
			//���� � ������ ���� ��������� �������� ������� �������� �������� ��� CurrentWeapon
			if (!MyInventoryComponent->CheckAmmoForWeapon(WeaponSettings.WeaponType, AvailableAmmoForWeapon))
			{
				result = false;
			}
			else
			{
				MyInventoryComponent->OnWeaponHaveRound.Broadcast(WeaponSettings.WeaponType);
			}
		}
	}

	return result;
}

int8 AWeaponDefault::GetAvailableAmmoForReload()
{
	int8 AvailableAmmoForWeapon = WeaponSettings.MaxRound;

	if (GetOwner())
	{
		UE_LOG(LogTemp, Error, TEXT("AWeaponDefault::CheckCanWeaponReload - OwnerName - %s"), *GetOwner()->GetName());
		UMyTDSGame_InventoryComponent* MyInventoryComponent = Cast<UMyTDSGame_InventoryComponent>(GetOwner()->GetComponentByClass(UMyTDSGame_InventoryComponent::StaticClass()));
		if (MyInventoryComponent)
		{


			//���� � ������ ���� ��������� �������� ������� �������� �������� ��� CurrentWeapon
			if (MyInventoryComponent->CheckAmmoForWeapon(WeaponSettings.WeaponType, AvailableAmmoForWeapon))
			{
				AvailableAmmoForWeapon = AvailableAmmoForWeapon;
			}
		}
	}

	return AvailableAmmoForWeapon;
}

//
void AWeaponDefault::TraceCheck()
{
	FHitResult HitResult;
	FVector Start = ShootLocation->GetComponentLocation();
	FVector End = Start + ShootLocation->GetForwardVector() * 2000.0f;
	TArray<AActor*> IgnoredActor;
	FCollisionQueryParams Params;

	if (GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECC_Visibility, Params, FCollisionResponseParams()))
	{
		UGameplayStatics::ApplyPointDamage(HitResult.GetActor(), WeaponSettings.WeaponDamage, Start, HitResult, NULL, this, NULL);
	}
	DrawDebugLine(GetWorld(), Start, End, FColor::Red, false, 5.0f, 0.f, 5.0f);
}
