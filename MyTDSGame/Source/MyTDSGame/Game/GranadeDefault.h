// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Game/ProjectileDefault.h"
#include "GranadeDefault.generated.h"

UCLASS()
class MYTDSGAME_API AGranadeDefault : public AProjectileDefault
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void ExplosionTimer(float DeltaTime);

	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

	virtual void ImpactProjectile() override;

	void Explosion();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion")
	bool ExplodeOnActorHit = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosion")
	bool TimerEnabled = false;

	//������� �������
	float ExplosionTimerValue = 0.0f;

	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = "Explosion")
	float TimeToExplode = 5.0f;


};
