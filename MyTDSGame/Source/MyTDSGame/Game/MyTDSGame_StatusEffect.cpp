// Fill out your copyright notice in the Description page of Project Settings.


#include "MyTDSGame_StatusEffect.h"
#include "../Character/MyTDSGame_HealthComponent.h"
#include "../Character/MyMyTDSGame_CharHealthComponent.h"
#include "../Interface/MyTDSGame_IGameActor.h"
#include "../Character/MyTDSGameCharacter.h"


bool UMyTDSGame_StatusEffect::InitializeObject(AActor* Actor,FName NameBoneHit)
{
	MyActor = Actor;


	IMyTDSGame_IGameActor* MyInterface = Cast<IMyTDSGame_IGameActor>(MyActor);
	if (MyInterface)
	{
		MyInterface->AddEffect(this);
	}

return true;
}

void UMyTDSGame_StatusEffect::DestroyObject()
{
	IMyTDSGame_IGameActor* MyInterface = Cast<IMyTDSGame_IGameActor>(MyActor);
	if (MyInterface)
	{
		MyInterface->RemoveEffect(this);
	}

	MyActor = nullptr;

	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

//Execute Once

bool UMyTDSGame_StatusEffect_ExecuteOnce::InitializeObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitializeObject(Actor, NameBoneHit);

	if (!isActivated)
	{
		ExecuteOnce();
	}
return true;
}

void UMyTDSGame_StatusEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UMyTDSGame_StatusEffect_ExecuteOnce::ExecuteOnce()
{
	if (MyActor)
	{
		UMyTDSGame_HealthComponent* MyHealthComponent = Cast<UMyTDSGame_HealthComponent>(MyActor->GetComponentByClass(UMyTDSGame_HealthComponent::StaticClass()));
		if (MyHealthComponent)
		{
			MyHealthComponent->ChangeHealthValue(Power);
		}
	}

	isActivated = true;

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_CooldownTimer, this, &UMyTDSGame_StatusEffect_ExecuteOnce::Cooldown, CooldownTimer, false);

}

void UMyTDSGame_StatusEffect_ExecuteOnce::Cooldown()
{
	isActivated = false;

	DestroyObject();

}


//Execute Timer

bool UMyTDSGame_StatusEffect_ExecuteTimer::InitializeObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitializeObject(Actor, NameBoneHit);


	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UMyTDSGame_StatusEffect_ExecuteTimer::DestroyObject, Timer, false);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UMyTDSGame_StatusEffect_ExecuteTimer::Execute, RateTime, true);

	if (ParticleEffect)
	{
		FName NameBoneToAttach = NameBoneHit;
		FVector Location;
		FRotator Rotation = FRotator::ZeroRotator;
		USceneComponent* MyMesh = Cast<USceneComponent> (MyActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));

		if (MyMesh)
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, MyMesh,
				NameBoneToAttach, Location, Rotation,
				EAttachLocation::SnapToTarget, false);
		}
		else
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, MyActor->GetRootComponent(),
				NameBoneToAttach, Location, Rotation,
				EAttachLocation::SnapToTarget, false);
		}

		
	}

return true;
}

void UMyTDSGame_StatusEffect_ExecuteTimer::DestroyObject()
{
	ParticleEmitter -> DestroyComponent();
	ParticleEmitter = nullptr;

	Super::DestroyObject();
}

void UMyTDSGame_StatusEffect_ExecuteTimer::Execute()
{
	if (MyActor)
	{
		UE_LOG(LogTemp, Warning, TEXT("UMyTDSGame_StatusEffect_ExecuteTimer::Execute"));
		UMyTDSGame_HealthComponent* MyHealthComponent = Cast<UMyTDSGame_HealthComponent>(MyActor->GetComponentByClass(UMyTDSGame_HealthComponent::StaticClass()));
		if (MyHealthComponent)
		{
			MyHealthComponent->ChangeHealthValue(Power);
		}
	}

}


// Temporary Execute
bool UMyTDSGame_StatusEffect_TemporaryExecute::InitializeObject(AActor* Actor, FName NameBoneHit)
{
		Super::InitializeObject(Actor, NameBoneHit);

		BoostTheshold = ConstHPMax - Boost;//����� ����� �������� 
		CoeffPer1Health = MaxAddBoostAmp / Boost;//�������� �� 1 ��
		
		if (!bIsActivated)
		{
			Execute();
		}

		

return true;
}

void UMyTDSGame_StatusEffect_TemporaryExecute::Execute()
{
	if (MyActor)
	{
		UE_LOG(LogTemp, Warning, TEXT("UMyTDSGame_StatusEffect_TemporaryExctue::Execute()"));

		UMyMyTDSGame_CharHealthComponent* MyCharHealthComponent = Cast<UMyMyTDSGame_CharHealthComponent>(MyActor->GetComponentByClass(UMyMyTDSGame_CharHealthComponent::StaticClass()));
		if (MyCharHealthComponent)
		{

			CurrentHealth = MyCharHealthComponent->GetCurrentHealth();
			//���� � ������ ������������ ��, �� ���� �������� ��� ������ �������
			if (CurrentHealth < BoostTheshold)
			{
				bIsActivated = true;
				MyCharHealthComponent->ChangeHealthValue(Boost);
			}
			//���� � ������ ���������� �� ��� �������� ������� Boost, �� ��� ������ � ���� ��, ��� ����������� Boost
			else
			{	//�������� �������, ��� �� ���� ������
				if (CurrentHealth >= BoostTheshold)
				{
					bIsActivated = true;

					Diff = ConstHPMax - CurrentHealth; //�� ������� �� ������ ������ ������������� 
					AddBoostAmpMultiplyer = Boost - Diff;//�� ������� ��� ������� �������� �� �������� �������
					ResultAddBoostAmp = CoeffPer1Health * AddBoostAmpMultiplyer;//�������� �������������� ��������� �������

					ResultBoost = Boost * (BaseBoostAmp + ResultAddBoostAmp);//�������� �������� �������

					MyCharHealthComponent->ChangeMaxHealthValue(ResultBoost);//������� ����������� ���������� ��

					MyCharHealthComponent->ChangeHealthValue(ResultBoost);//����� �������� ��� ������� �������

					
				}
			}
			
			GetWorld()->GetTimerManager().SetTimer(TimerHandle_CooldownTimer, this,
											&UMyTDSGame_StatusEffect_TemporaryExecute::DurationEnded, CooldownTimer, false);
		}
	}
}

void UMyTDSGame_StatusEffect_TemporaryExecute::DestroyObject()
{

	Super::DestroyObject();
}

void UMyTDSGame_StatusEffect_TemporaryExecute::DurationEnded()
{
	if (MyActor)
	{
		UE_LOG(LogTemp, Warning, TEXT("UMyTDSGame_StatusEffect_TemporaryExctue::DurationEnded()"));

		UMyMyTDSGame_CharHealthComponent* MyCharHealthComponent = Cast<UMyMyTDSGame_CharHealthComponent>(MyActor->
																		GetComponentByClass(UMyMyTDSGame_CharHealthComponent::StaticClass()));

		if (MyCharHealthComponent)
		{

			MyCharHealthComponent->ChangeMaxHealthValue(-ResultBoost);

			MyCharHealthComponent->ChangeHealthValue(0);

			ResultBoost = 0; // ���������� ����������

		}
	}

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this,
		&UMyTDSGame_StatusEffect_TemporaryExecute::Cooldown, DurationTimer, false);
}

void UMyTDSGame_StatusEffect_TemporaryExecute::Cooldown()
{
	bIsActivated = false;

	DestroyObject();
}

bool UMyTDSGame_StatusEffect_TemporaryExecute::GetCooldown()
{
	bool Cooldown = bIsActivated;

	return Cooldown;
}



//������ �����
bool UMyTDSGame_StatusEffect_TemporaryStun::InitializeObject(AActor* Actor, FName NameBoneHit)
{
	UE_LOG(LogTemp, Warning, TEXT("UMyTDSGame_StatusEffect_TemporaryStun::InitializeObject()"));

	Super::InitializeObject(Actor, NameBoneHit);

	if (!bIsActivated)
	{
		Execute();
	}



	return true;
}

void UMyTDSGame_StatusEffect_TemporaryStun::Execute()
{
	//��������� �������� ������� ������ ���������� ����� ����� ������� �������

	bIsActivated = true;

	if (EffectAudio)
	{
		SoundEmitter = UGameplayStatics::SpawnSoundAtLocation(GetWorld(), EffectAudio, MyActor->GetActorLocation(), MyActor->GetActorRotation());
	}

	if (StunAnim)
	{
	
	}

	if (MyActor)
	{
		AMyTDSGameCharacter* MyChar = Cast<AMyTDSGameCharacter>(MyActor);

		if (MyChar)
		{
			MyChar->CharStunned();

			MyChar->StunStarted();

			UE_LOG(LogTemp, Warning, TEXT("UMyTDSGame_StatusEffect_TemporaryStun::Execute() - CharStunned & StunStarted"));
		}
		
		
	}

	//� ����� ��������� ������, �� ��������� �������� ���������� ������� ������� �������� ��� �������� ������� ������ ���������
	//�� ��������� �������� �������

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_StunTimer,this,
									&UMyTDSGame_StatusEffect_TemporaryStun::DurationEnded, DurationTimer, false);

}

void UMyTDSGame_StatusEffect_TemporaryStun::DestroyObject()
{
	UE_LOG(LogTemp, Warning, TEXT("UMyTDSGame_StatusEffect_TemporaryStun::DestroyObject()"));

	Super::DestroyObject();
}

void UMyTDSGame_StatusEffect_TemporaryStun::DurationEnded()
{
	UE_LOG(LogTemp, Warning, TEXT("UMyTDSGame_StatusEffect_TemporaryStun::DurationEnded()"));
	
	AMyTDSGameCharacter* MyChar = Cast<AMyTDSGameCharacter>(MyActor);
	MyChar->StunEnded();

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_CooldownTimer, this,
		&UMyTDSGame_StatusEffect_TemporaryStun::Cooldown, CooldownTimer, false);
}

void UMyTDSGame_StatusEffect_TemporaryStun::Cooldown()
{
	UE_LOG(LogTemp, Warning, TEXT("UMyTDSGame_StatusEffect_TemporaryStun::Cooldown()"));

	bIsActivated = false;

	DestroyObject();
}

bool UMyTDSGame_StatusEffect_TemporaryStun::GetCooldown()
{
	bool Cooldown = bIsActivated;

	return Cooldown;
}



// Temporary invulnerability

bool UMyTDSGame_StatusEffect_TempoararyInvulnerability::InitializeObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitializeObject(Actor, NameBoneHit);

	if (!bIsActivated)
	{
		Execute();
	}

	return true;
}

void UMyTDSGame_StatusEffect_TempoararyInvulnerability::DestroyObject()
{
	

	Super::DestroyObject();

}

void UMyTDSGame_StatusEffect_TempoararyInvulnerability::Execute()
{
	UE_LOG(LogTemp, Warning, TEXT("UMyTDSGame_StatusEffect_TempoararyInvulnerability::Execute()"));

	//���������� ���� ���������� �� UMyTDSGame_HealthComponent
	UMyTDSGame_HealthComponent* MyHealthComp = Cast<UMyTDSGame_HealthComponent>(MyActor->GetComponentByClass(UMyTDSGame_HealthComponent::StaticClass()));

	bIsActivated = true;
	if (MyHealthComp)
	{
		PreviousCoeff = MyHealthComp->DamageCoef;
		MyHealthComp->DamageCoef = TemporaryDamageCoeff;
	}

	if (EffectAudio)
	{
		SoundEmitter = UGameplayStatics::SpawnSoundAtLocation(GetWorld(),EffectAudio, MyActor->GetActorLocation(),MyActor->GetActorRotation());
	}

	if (ParticleEffect)
	{
		FName AreaAttachComponent;
		FVector AreaLocation = FVector::ZeroVector;
		FRotator AreaRotation = FRotator::ZeroRotator;
		USceneComponent* ComponnetToAttach = MyActor->GetRootComponent();


		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect,
				ComponnetToAttach,
				AreaAttachComponent,
				AreaLocation, AreaRotation, EAttachLocation::SnapToTargetIncludingScale,
				true);

	}

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this,
		&UMyTDSGame_StatusEffect_TempoararyInvulnerability::DurationEnded, DurationTimer, false);


	
}

void UMyTDSGame_StatusEffect_TempoararyInvulnerability::Cooldown()
{
	UE_LOG(LogTemp, Warning, TEXT("UMyTDSGame_StatusEffect_TempoararyInvulnerability::Cooldown()"));

	bIsActivated = false;

	DestroyObject();
}

void UMyTDSGame_StatusEffect_TempoararyInvulnerability::DurationEnded()
{
	UE_LOG(LogTemp, Warning, TEXT("UMyTDSGame_StatusEffect_TempoararyInvulnerability::DurationEnded()"));

	if (EffectEndAudio)
	{
		SoundEmitter = UGameplayStatics::SpawnSoundAtLocation(GetWorld(), EffectEndAudio, MyActor->GetActorLocation(), MyActor->GetActorRotation());
	}

	if (ParticleEffect)
	{		
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}

	UMyTDSGame_HealthComponent* MyHealthComp = Cast<UMyTDSGame_HealthComponent>(MyActor->GetComponentByClass(UMyTDSGame_HealthComponent::StaticClass()));
	if (MyHealthComp)
	{
		MyHealthComp->DamageCoef = PreviousCoeff;
	}

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this,
		&UMyTDSGame_StatusEffect_TempoararyInvulnerability::Cooldown, CooldownTimer, false);
}


//Temporary Damage Area
bool UMyTDSGame_StatusEffect_TempoararyDamageArea::InitializeObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitializeObject(Actor, NameBoneHit);

	if (!bIsActivated)
	{
		//������ ������������ �������
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this,
									&UMyTDSGame_StatusEffect_TempoararyDamageArea::DurationEnded, DurationTimer, false);

		//����� Execute � �������������� DamageTimer
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTick, this,
										&UMyTDSGame_StatusEffect_TempoararyDamageArea::Execute, DamageTimer, true);

	}
	
	bIsActivated = true;

	FName AreaAttachComponent;
	FVector AreaLocation;
	FRotator AreaRotation = FRotator::ZeroRotator;
	if (EffectAudio)
	{
		SoundEmitter = UGameplayStatics::SpawnSoundAttached(EffectAudio,
			MyActor->GetRootComponent(), AreaAttachComponent,
			AreaLocation, AreaRotation, EAttachLocation::SnapToTarget,
			true);
	}

	return true;
}

void UMyTDSGame_StatusEffect_TempoararyDamageArea::DestroyObject()
{
	Super::DestroyObject();
}

void UMyTDSGame_StatusEffect_TempoararyDamageArea::Execute()
{
	//Damage
	FVector Location = MyActor->GetActorLocation();

	IgnoredActors.Add(MyActor);

	UGameplayStatics::ApplyRadialDamage(GetWorld(), Damage, Location, DamageRadius, NULL, IgnoredActors, MyActor, nullptr, true);


	FName AreaAttachComponent;
	FVector AreaLocation = FVector::ZeroVector;
	FRotator AreaRotation = FRotator::ZeroRotator;
	USceneComponent* ComponnetToAttach = MyActor->GetRootComponent();
	

	if (ParticleEffect)
	{
		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect,
																ComponnetToAttach	,
																AreaAttachComponent,
																AreaLocation, AreaRotation, EAttachLocation::SnapToTarget,
																true);
	}
	//Debug
	/*
	FVector CircleLocation = MyActor->GetActorLocation();
	const FVector Z = FVector(0.0f, 0.0f, 20.0f);

	CircleLocation.operator+= (Z);

	FVector Yaxis = FVector(1.0f, 0.0f, 0.0f);
	FVector Zaxis = FVector(0.0f, 1.0f, 0.0f);

	float R = 1.0f;
	float G = 0.0f;
	float B = 0.0f;
	float A = 1.0f;
	const FColor Color = FColor(R, G, B, A);

	int32 Segments = 40;

	float Lifetime = 0.3f;

	uint8 Depth = 0;

	DrawDebugCircle(GetWorld(), CircleLocation, DamageRadius, Segments, Color, false, Lifetime, Depth, 20.0f, Yaxis, Zaxis, true);
	*/
}

void UMyTDSGame_StatusEffect_TempoararyDamageArea::DurationEnded()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;

	SoundEmitter->Stop();
	SoundEmitter->DestroyComponent();
	SoundEmitter = nullptr;


	GetWorld()->GetTimerManager().ClearTimer(TimerHandle_EffectTick);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_CooldownTimer, this,
									&UMyTDSGame_StatusEffect_TempoararyDamageArea::Cooldown, CooldownTimer, false);
}

void UMyTDSGame_StatusEffect_TempoararyDamageArea::Cooldown()
{
	bIsActivated = false;

	DestroyObject();
}
