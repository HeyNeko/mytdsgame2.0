// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Actor.h"
#include "DrawDebugHelpers.h"


#include "MyTDSGame_StatusEffect.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBoostActivated);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBoostDeActivated);
/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class MYTDSGAME_API UMyTDSGame_StatusEffect : public UObject
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "StatusBoost")
	FOnBoostActivated OnBoostActivated;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "StatusBoost")
	FOnBoostDeActivated OnBoostDeActivated;

	virtual bool InitializeObject(AActor* Actor, FName NameBoneHit);

	virtual void DestroyObject();

	//������ ������� ������ ���� PhysicalSurface �� ������� ����� ����� ��������� ���� ������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category  = "Settings")
		TArray <TEnumAsByte<EPhysicalSurface>> PossibleInteractionSurface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		bool bIsStackable = false;

	AActor* MyActor = nullptr;

};

//������ ������� ������ ��������� ���� ���
UCLASS()
class MYTDSGAME_API UMyTDSGame_StatusEffect_ExecuteOnce : public UMyTDSGame_StatusEffect
{
	GENERATED_BODY()

public:

	bool InitializeObject(AActor* Actor, FName NameBoneHit) override;

	void DestroyObject() override;

	virtual void ExecuteOnce();

	virtual void Cooldown();

	bool isActivated = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings Execute Once")
		float Power = 20.0f;

	//����� �����������
	FTimerHandle TimerHandle_CooldownTimer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings TemporaryExecute")
		float CooldownTimer = 5.0f;


	//����������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects for player")
		USoundWave* EffectAudio = nullptr;

	UAudioComponent* SoundEmitter = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects for player")
		UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;

};

//������ ������������� Timer � ���������� Execute ��� � RateTimer �������
UCLASS()
class MYTDSGAME_API UMyTDSGame_StatusEffect_ExecuteTimer  : public UMyTDSGame_StatusEffect
{
	GENERATED_BODY()

public:

	bool InitializeObject(AActor* Actor, FName NameBoneHit) override;

	void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings Execute Timer")
		float Power = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings Execute Timer")
		float Timer = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings Execute Timer")
		float RateTime = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;

	FTimerHandle TimerHandle_EffectTimer;

	//����������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects for player")
		USoundWave* EffectAudio = nullptr;

	UAudioComponent* SoundEmitter = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects for player")
		UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;
};


//��������� ������ ������� ��������� ���� ��� � ����� ��������
UCLASS()
class MYTDSGAME_API UMyTDSGame_StatusEffect_TemporaryExecute : public UMyTDSGame_StatusEffect
{
	GENERATED_BODY()

public:
	//������ - ����
	//���� ����� ��������� ������� �� ���������, �� ���� � ������ ������������� ����� � ���������
	// �� ������ ��� Theshold, �� ���� ��������� �����������
	// ������ ��� 100 �� ����� ������������ ������������� BaseBoostAmp + MaxAddBoostAmp
	// ��� theshold ����� ������ BaseBoostAmp �������������
	// ������ ��� ����������� �� ���� ����� ��������� 100 �� �� ��������� ��������
	// ����� ��������� �����, ���� �� ����� �� 100, �� �������� �� 100
	// � ���� ���, �� ��������� ��� ������� �������, � ��������� ��������������

	//���������� ��� ���������� ���������� �����

	// ���� - ������ �����������
	bool bIsActivated = false;

	//����������� �� ��������� ��� �����
	const float ConstHPMax = 100.0f;

	//������� �������� ������
	float CurrentHealth = 0.0f;

	//������������� ��
	float Diff = 0.0f;

	//������������� �����
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings TemporaryExecute")
	float Boost = 20.0f;
	//��������� ����
	float ResultBoost = 0.0f;

	//����� ��� ������� ���������� Boost �� ������� �� ���� 100
	float BoostTheshold = 0.0f;

	//���������� ����������� ������������� ��������������� ������� �� ������ 1�� 
	//���� BoostThershold ������
	float CoeffPer1Health = 0.0f;

	//������� ��������� ������������� �������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings TemporaryExecute")
	float BaseBoostAmp = 1.5f;

	//������������ �������������� ��������� ������������� �������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings TemporaryExecute")
	float MaxAddBoostAmp = 1.0f;

	//��������� ��� AddBoostAmp ������� ��� ����, ��� ������ �� � ������
	float AddBoostAmpMultiplyer = 0.0f;

	//������� �������������� ���������
	float ResultAddBoostAmp = 0.0f;



	//�������
	bool InitializeObject(AActor* Actor, FName NameBoneHit) override;

	void DestroyObject() override;

	virtual void Execute();

	virtual void DurationEnded();

	virtual void Cooldown();

	virtual bool GetCooldown();

	//����� �� ����� �������� ����������
	FTimerHandle TimerHandle_EffectTimer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings TemporaryExecute")
		float DurationTimer = 5.0f;

	//����� �����������
	FTimerHandle TimerHandle_CooldownTimer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings TemporaryExecute")
		float CooldownTimer = 5.0f;



	//����������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects for player")
		USoundWave* EffectAudio = nullptr;

	UAudioComponent* SoundEmitter = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects for player")
		UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;
};


//��������� ������ �����
UCLASS()
class MYTDSGAME_API UMyTDSGame_StatusEffect_TemporaryStun : public UMyTDSGame_StatusEffect
{
	GENERATED_BODY()

public:
	//������ ������ - ����
	//���� � Flesh �� ��������� ����� �������� ���������� � ������ ����������� ��������, ��� ���� ����� �������� ����,
	//�� �� ����� ������������ ������, ��������

	//���������� ��� ���������� ���������� �����

	// ���� - ������ �����������
	bool bIsActivated = false;


	//�������
	bool InitializeObject(AActor* Actor, FName NameBoneHit) override;

	void DestroyObject() override;

	virtual void Execute();

	virtual void DurationEnded();

	virtual void Cooldown();

	virtual bool GetCooldown();

	//����� �� ����� �������� ���������� �������
	FTimerHandle TimerHandle_StunTimer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TemporaryStun")
		float DurationTimer = 2.0f;

	//����� �����������(����� �� ������� � ChainStun) 
	FTimerHandle TimerHandle_CooldownTimer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TemporaryStun")
		float CooldownTimer = 1.0f;



	//����������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects for player")
		USoundWave* EffectAudio = nullptr;

	UAudioComponent* SoundEmitter = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects for player")
		UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects for player")
		UAnimMontage* StunAnim = nullptr;
};


//��������� ������ ������� ��� ������������
UCLASS()
class MYTDSGAME_API UMyTDSGame_StatusEffect_TempoararyInvulnerability : public UMyTDSGame_StatusEffect
{
	GENERATED_BODY()

public:
	//������ - ������������
	//�� ����� ��� ����������� ����� = 0
	
	//����������
	bool bIsActivated = false;
	float TemporaryDamageCoeff = 0;
	float PreviousCoeff = 0.f;


	//�������
	bool InitializeObject(AActor* Actor, FName NameBoneHit) override;

	void DestroyObject() override;

	virtual void Execute();

	virtual void Cooldown();

	virtual void DurationEnded();

	//����� �� ����� �������� ����������
	FTimerHandle TimerHandle_EffectTimer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings TempoararyInvulnerability")
		float DurationTimer = 5.0f;

	//����� �����������
	FTimerHandle TimerHandle_CooldownTimer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings TempoararyInvulnerability")
		float CooldownTimer = 5.0f;



	//����������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects for player")
		USoundWave* EffectAudio = nullptr;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects for player")
		USoundWave* EffectEndAudio = nullptr;

	UAudioComponent* SoundEmitter = nullptr;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects for player")
		UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;
};

//��������� ������ ������� ������ ������� ������ ��������� ��������� ���� � ��������� ��������
UCLASS()
class MYTDSGAME_API UMyTDSGame_StatusEffect_TempoararyDamageArea : public UMyTDSGame_StatusEffect
{
	GENERATED_BODY()

public:
	//������ - ������������
	//�� ����� ��� ����������� ����� = 0

	//����������

	bool bIsActivated = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings TempoararyDamageArea")
	float Damage = 10.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings TempoararyDamageArea")
	float DamageRadius = 500;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings TempoararyDamageArea")
	TArray<AActor*> IgnoredActors;
	

	//�������
	bool InitializeObject(AActor* Actor, FName NameBoneHit) override;

	void DestroyObject() override;

	virtual void Execute();

	virtual void Cooldown();

	virtual void DurationEnded();

	//����� �� ����� �������� ����������
	FTimerHandle TimerHandle_EffectTimer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings TempoararyDamageArea")
		float DurationTimer = 5.0f;

	//������� ��������� �����
	FTimerHandle TimerHandle_EffectTick;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings TempoararyDamageArea")
		float DamageTimer = 0.3f;

	//����� �����������
	FTimerHandle TimerHandle_CooldownTimer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings TempoararyDamageArea")
		float CooldownTimer = 5.0f;



	//����������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects for player")
		USoundWave* EffectAudio = nullptr;

	UAudioComponent* SoundEmitter = nullptr;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects for player")
		UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;

};