// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Interface/MyTDSGame_IGameActor.h"
#include "../Game/MyTDSGame_StatusEffect.h"
#include "MyTDSGame_EnviromentStructure.generated.h"

UCLASS()
class MYTDSGAME_API AMyTDSGame_EnviromentStructure : public AActor, public IMyTDSGame_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyTDSGame_EnviromentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfaceType() override;

	//Effects
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	TArray<UMyTDSGame_StatusEffect*> Effects;


	//Effects Methods Override
	 
	TArray<UMyTDSGame_StatusEffect*> GetAllCurrentEffects() override;

	void RemoveEffect(UMyTDSGame_StatusEffect* RemoveEffect) override;

	void AddEffect(UMyTDSGame_StatusEffect* AddEffect) override;
};
