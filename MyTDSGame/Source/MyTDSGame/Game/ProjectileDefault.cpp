
// Fill out your copyright notice in the Description page of Project Settings.


#include "../Game/ProjectileDefault.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h" 


// Sets default values
AProjectileDefault::AProjectileDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Collision SPhere
	//Creating Implimitation of Sphere and setting its rafius
	BulletColisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision SPhere"));
	BulletColisionSphere -> SetSphereRadius(5.f);

	//Adjusting pheres settings
	//Attaching function OnComponent Hit to BulletSphere
	BulletColisionSphere->OnComponentHit.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereHit);
	//Attaching function BeginOvelap to Bulelt shpere( making it trigger?)
	BulletColisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereBeginOverlap);
	//
	BulletColisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereEndOverlap);
	//
	BulletColisionSphere->bReturnMaterialOnMove = true; //Checking the box of returning physMaterial on hit
	//
	BulletColisionSphere->SetCanEverAffectNavigation(false); // affect AI?
	//
	RootComponent = BulletColisionSphere;


	//Creating Bullet mesh 
	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);

	//Bullet FX
	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

	//Projectile Movement
	//Creating Menu
	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet Projectile Movement"));
	//Adjusting Settings
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	BulletProjectileMovement->InitialSpeed = 1.f;
	BulletProjectileMovement->MaxSpeed = 0.f;
	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = true;


}

// Called when the game starts or when spawned
void AProjectileDefault::BeginPlay()
{
	Super::BeginPlay();

	//���������� ������� OnComponentHit? �� �� �����, ��� � ���������� ��������� ����?
	BulletColisionSphere->OnComponentHit.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereHit);

	//���������� OnComponentBeginOverap?
	BulletColisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereBeginOverlap);

	//���������� OnCompponentEndOverlap
	BulletColisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereEndOverlap);

	
}

// Called every frame
void AProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectileDefault::InitProjectile(FProjectileInfo InitParam)
{
	//����� ��������� ��������� � ��������� ProjectileMovement ������� ��� �������� � SceneComponentn
	BulletProjectileMovement->InitialSpeed = InitParam.ProjectileInitialSpeed;
	BulletProjectileMovement->MaxSpeed = InitParam.ProjectileMaxSpeed;
	this->SetLifeSpan(InitParam.ProjectileLifeTime);

	ProjectileSettings = InitParam;
}

void AProjectileDefault::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//���� �������� ����� ������������� � ������ ������

	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

		//�������� ������� �� ������ ������� ������ ������������ ��� Hit
		if (ProjectileSettings.HitDecals.Contains(mySurfacetype))
		{
			UMaterialInterface* MyMaterial = ProjectileSettings.HitDecals[mySurfacetype];

			if (MyMaterial && OtherComp)
			{
				UGameplayStatics::SpawnDecalAttached(MyMaterial, FVector(20.0f), OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
			}
				
		}

		//�������� ������� �� ���������� ������� ������� ������ ������������ ��� Hit
		if (ProjectileSettings.HitFXs.Contains(mySurfacetype))
		{
			UParticleSystem* MyParticle = ProjectileSettings.HitFXs[mySurfacetype];

			if (MyParticle)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), MyParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
			}
		}

		//�������� ������� �� ����� ������� ������ ������������ ��� Hit
		if (ProjectileSettings.HitSound)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSettings.HitSound, Hit.ImpactPoint);
		}

		//���� ����� �������� ������ � ������ Actor � � ���� ���� PhysMaterial
		UTypes::AddStatusEffectBySurfaceType(OtherActor, Hit.BoneName, ProjectileSettings.StatusEffect, mySurfacetype);
	}



	UGameplayStatics::ApplyDamage(OtherActor, ProjectileSettings.ProjectileDamage, GetInstigatorController(), this, NULL);

	//����� ����� ����������� ��������� ������� �� � ��
	UAISense_Damage::ReportDamageEvent(GetWorld(), Hit.GetActor(), GetInstigator(), ProjectileSettings.ProjectileDamage, Hit.Location, Hit.Location);

	ImpactProjectile();

}

void AProjectileDefault::BulletCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void AProjectileDefault::BulletCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AProjectileDefault::ImpactProjectile()
{
	this->Destroy();
}
