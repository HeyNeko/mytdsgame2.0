// Fill out your copyright notice in the Description page of Project Settings.


#include "GranadeDefault.h"
#include "Kismet/GameplayStatics.h"

// Called when the game starts or when spawned
void AGranadeDefault::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGranadeDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ExplosionTimer(DeltaTime);

}

//������� ��������� ������ �������� �� ������� ImpactProjectile. �� ���������� ������� �������� ������� Explosion
void AGranadeDefault::ExplosionTimer(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (ExplosionTimerValue > TimeToExplode)
		{
			Explosion();

		}
		else
		{
			ExplosionTimerValue = ExplosionTimerValue + DeltaTime;
		}

	}
}


//��� ��������� �������� �� �������� �������� ������������ -> ������ ����������� ImpactProjectile � ApplyDamage
void AGranadeDefault::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!ExplodeOnActorHit)
	{
		Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
	}
	else
	{
		if (OtherActor && Hit.PhysMaterial.IsValid())
		{
			Explosion();
		}
	}
	//�������� ��������� ����� ��� �������� � ������ � ��������� ������� �� ������ ������������?
}

//���������� ������� Projectile. ������ ��������������� ��������� ������
void AGranadeDefault::ImpactProjectile()
{
	TimerEnabled = true;
}

void AGranadeDefault::Explosion()
{
	TimerEnabled = false;

	if (ProjectileSettings.ExplosionFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSettings.ExplosionFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}

	if (ProjectileSettings.ExplosionSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSettings.ExplosionSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;

	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSettings.ExplosionMaxDamage,
		(ProjectileSettings.ExplosionMaxDamage * 0.2f),
		GetActorLocation(),
		ProjectileSettings.ProjectileMinRadiusDamage,
		ProjectileSettings.ProjectileMaxRadiusDamage,
		ProjectileSettings.ExplosionDamageFallOff,
		NULL,
		ProjectileSettings.IgnoredExplosionActors,
		this,
		nullptr);

	this -> Destroy();
}

