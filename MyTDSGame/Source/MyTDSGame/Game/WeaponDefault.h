// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "../FunctionLibrary/Types.h"
#include "../Game/ProjectileDefault.h"
#include "Engine/World.h"
#include "../Interface/MyTDSGame_IGameActor.h"
#include "../Game/MyTDSGame_StatusEffect.h"
#include "WeaponDefault.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam (FOnWeaponFireStart, UAnimMontage*, AnimFireStart);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam (FOnWeaponReloadStart, UAnimMontage*, Anim);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams (FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);


UCLASS()
class MYTDSGAME_API AWeaponDefault : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponFireStart OnWeaponFireStart;

	FOnWeaponReloadStart OnWeaponRealoadStart;

	FOnWeaponReloadEnd OnWeaponReloadEnd;
	

	//Creating SceneComponent
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;

	//Creating Skeletal Mesh of Weapon
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;

	//Creating Static Mesh of Weapon
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	//Creating Arrow component wich show where the projectile will spawn and in which direction it will fly

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ShootLocation = nullptr;
	//Creating menu structure from types.h
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		FWeaponInfo WeaponSettings;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponInfo")
		FAdditionalWeaponInfo AdditionalWeaponInfo;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UParticleSystemComponent* WeaponShells = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UParticleSystemComponent* WeaponMagazine = nullptr;

	UFUNCTION()
		void TraceCheck();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//A separate Tick funciton but for weapon fire?
	void FireTick(float DeltaTime);

	void ReloadTick(float DeltaTime); // ������ ?

	void DispersionTick(float DeltaTime); //

	//Functions check if weapon has static mesh or skeletal mesh
	void WeaponInit();

	//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		bool WeaponIsFiring = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
		bool WeaponReloading = false;


	//Function to change Weapon State
	UFUNCTION(BlueprintCallable)
		void SetWeaponStateIsFiring(bool bIsFire);

	//
	bool CheckWeaponCanFire();

	//
	FProjectileInfo GetProjectile();

	//
	void Fire();

	//
	void UpdateStateWeapon(EMovementState NewMovementState);
	void ChangeDispersionByShot();
	float GetCurrentDispersion() const;
	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;

	FVector GetFireEndLocation() const;
	int8 GetNumberProjectileByShot() const;
	//
	void ChangeDispersion();

	//Timers
	float FireTimer = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
		float ReloadTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic Debug")
		float ReloadTime = 0.0f;

	//Flags
	bool BlockFire = false;

	//Dispersion
	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	FVector ShootEndLocation = FVector(0);

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound();

	void initReload();
	void FinishReload();
	void CancelReload();

	bool CheckCanWeaponReload();

	int8 GetAvailableAmmoForReload ();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool ShowDebug = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		float SizeVectorToChangeShootDirectionLogic = 100.0f;

};
