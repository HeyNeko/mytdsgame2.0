// Fill out your copyright notice in the Description page of Project Settings.

#include "../Game/MyTDSGame_EnviromentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
AMyTDSGame_EnviromentStructure::AMyTDSGame_EnviromentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyTDSGame_EnviromentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyTDSGame_EnviromentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface AMyTDSGame_EnviromentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;

	UStaticMeshComponent* MyMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));

	if (MyMesh)
	{
		UMaterialInterface* MyMaterial =	MyMesh->GetMaterial(0);

		if (MyMaterial)
		{
			Result = MyMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}

	return Result;
}


TArray<UMyTDSGame_StatusEffect*> AMyTDSGame_EnviromentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void AMyTDSGame_EnviromentStructure::RemoveEffect(UMyTDSGame_StatusEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void AMyTDSGame_EnviromentStructure::AddEffect(UMyTDSGame_StatusEffect* AddEffect)
{
	Effects.Add(AddEffect);
}