// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "../Game/MyTDSGame_StatusEffect.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName = "Aim State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State"),
};

//WeaponType
UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	Pistol UMETA(DisplayName = "Pistol"),
	Rifle UMETA(DisplayName = "Rifle"),
	ShotGun UMETA(DisplayName = "ShotGun"),
	SniperRifle UMETA(DispayName = "SniperRifle"),
	GrenadeLauncher UMETA(DisplayName = "GrenadeLauncher")
};

//Character Speed
USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float AimSpeed = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float WalkSpeed = 400.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float RunSpeed = 600.0f;
};

//Projectile Info
USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Setting")
		TSubclassOf <class AProjectileDefault> Projectile = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Setting")
		float ProjectileDamage = 20.0f;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileSetting")
		float ProjectileLifeTime = 20.0f;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileSetting")
		float ProjectileInitialSpeed = 2000.0f;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileSetting")
		float ProjectileMaxSpeed = 2000.0f;

	//Setting material of Decal on moment of Hit
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileFX")
		TMap<TEnumAsByte <EPhysicalSurface>, UMaterialInterface*> HitDecals;

	//Setting Sound on moment of Hit
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileFX")
		USoundBase* HitSound = nullptr;

	//FX Setting on moment of Hit
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileFX")
		TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;

	//Settings for AOE projectiles
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileFX")
		UParticleSystem* ExplosionFX = nullptr;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileFX")
		USoundBase* ExplosionSound = nullptr;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "StatusEffect")
		TSubclassOf<UMyTDSGame_StatusEffect> StatusEffect = nullptr;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileExplosionSetting")
		float ProjectileMaxRadiusDamage = 500.0f;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileExplosionSetting")
		float ProjectileMinRadiusDamage = 100.0f;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileExplosionSetting")
		float ExplosionMaxDamage = 40.0f;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileExplosionSetting")
		float ExplosionDamageFallOff = 5.0f;

	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = "ProjectileExplosionSetting")
		TArray <AActor*> IgnoredExplosionActors;

};

//Weapon Dispersion
USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

		//Character Aiming
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimingDispersion_AimMax = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimingDispersion_AimMin = 0.3f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimingDispersion_AimRecoil = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimingDispersion_DispersionReduction = 0.3f;

	//Character Aiming while walking

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimWalkDispersion_AimMax = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimAWalkDispersion = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimWalkDispersion_AimRecoil = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimWalkDispersion_DispersionReduction = 0.4f;

	//Character just Walking
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float WalkDispersion_AimMax = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float WalkDispersion_AimMin = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float WalkDispersion_AimRecoil = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float WalkDispersion_DispersionReduction = 0.2f;

	//Character runing (Probably shouldn't be able to shooot
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float RunDispersion_AimMax = 10.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float RunDispersion_AimMin = 4.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float RunDispersion_AimRecoil = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float RunDispersion_DispersionReduction = 0.1f;
};

//Additional Weapon Info
USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Stats")
		int32 Round = 0;
};


//Weapon Info
USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
		TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
		float RateOfFire = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
		float ReloadTime = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
		int32 MaxRound = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
		int32 NumberProjectileByShot = 1;

	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		FWeaponDispersion DispersionWeapon;

	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundFireWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundReloadWeapon = nullptr;

	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		UParticleSystem* EffectFireWeapon = nullptr;

	//
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		FProjectileInfo ProjectileSettings;
	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		float WeaponDamage = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		float DistanceTrace = 2000.0f;
	//

	//Decal for OnHit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitEffect")
		UDecalComponent* DecalOnHit = nullptr;

	//Aniamtion triggering
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
		UAnimMontage* AnimCharFire = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
		UAnimMontage* AnimCharReload = nullptr;

	//Meshes for bullets and magazine
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		UParticleSystem* MagazineDrop = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		UParticleSystem* ShellBullets = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AdditionalWeaponInfo")
		FAdditionalWeaponInfo AdditionalWeaponInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		UTexture2D*  WeaponIcon =  nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		UTexture2D* AmmoIcon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		EWeaponType WeaponType = EWeaponType::Pistol;

};

//Weapon SLot
USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot");
	FName NameItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot");
	FAdditionalWeaponInfo AdditionalWeaponInfo;
};

//Ammo SLot
USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot");
	EWeaponType WeaponType = EWeaponType::Pistol;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot");
	int32 Cout = 12;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot");
	int32 MaxCout = 12;
};

//Drop SLot
USTRUCT(BlueprintType)
struct FDropWeapon : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon");
	UStaticMesh* WeaponStaticMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWEapon");
	USkeletalMesh* WeaponSkeletalMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon");
	FWeaponSlot WeaponInfo;
};

//Drop Item
USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase
{
	GENERATED_BODY()

		///Index Slot by Index Array
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
		UStaticMesh* WeaponStaticMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
		USkeletalMesh* WeaponSkeletMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
		FWeaponSlot WeaponInfo;
};

UCLASS()
class MYTDSGAME_API UTypes : public  UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	static void AddStatusEffectBySurfaceType(AActor* TakeEffectActor,FName NameBoneHit, TSubclassOf<UMyTDSGame_StatusEffect> AddStatusEffectClass, EPhysicalSurface SurfaceType);
};