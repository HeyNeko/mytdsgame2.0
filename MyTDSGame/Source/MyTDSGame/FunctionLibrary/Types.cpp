// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "../Interface/MyTDSGame_IGameActor.h"
#include "../MyTDSGame.h"

//��������� Actor �� �������� ����� �������� ������; StatusEffect ������� ������ � �� �� ������ C++ ������ � SurfaceType �� ������� ����� �������� ������
void UTypes::AddStatusEffectBySurfaceType(AActor* TakeEffectActor, FName NameBoneHit, TSubclassOf<UMyTDSGame_StatusEffect> AddStatusEffectClass, EPhysicalSurface SurfaceType)
{
	//���� Surface �� ���������, � ����� �� �������� ������ ����������� ������ � ������ ������ ����������
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddStatusEffectClass)
	{
		//�������� ���������� ���� ������ ������ 
		UMyTDSGame_StatusEffect* MyStatus_Effect = Cast<UMyTDSGame_StatusEffect>(AddStatusEffectClass->GetDefaultObject());
		if (MyStatus_Effect)
		{
			////����, ��� ���� ���������� Surface - �� �������� false
			bool bHavePossibleSurface = false;
			int8 i = 0;

			//�������� �� ���� ��������� SurfaceType ��������� � BP �������
			while (i < MyStatus_Effect->PossibleInteractionSurface.Num() && !bHavePossibleSurface)
			{
				//���� Surface ��������� � BP ������ � Surface � Actor �� �������� ������ ������������� ������  
				if (MyStatus_Effect->PossibleInteractionSurface[i] == SurfaceType)
				{
					//����, ��� ���� ���������� Surface
					bHavePossibleSurface = true;

					//����, ��� ������ ������ ����� ��������� � Actor - ���������� false
					bool bCanAddEffect = false;

					//��������� ��������� � BP, ����� �� ������ ���� ������� ��������� ���
					if (!MyStatus_Effect->bIsStackable)//���� ���
					{	
						//������ ������ � ����� ������ - StatusEffect
						int8 j = 0;
						TArray<UMyTDSGame_StatusEffect*> CurrentEffects;

						//������ Cast �� Interface �  Actor � �������� ������ ����������� ������ 
						IMyTDSGame_IGameActor* MyInterface = Cast<IMyTDSGame_IGameActor>(TakeEffectActor);

						//���� ������ Actor ����� Interface
						if (MyInterface)
						{
							//���������� � ������ �������� ����� ������ - ���� ������ � ���������� Actor �������
							//������� ������ ������ 1 ������������� ������� StatusEffect 
							CurrentEffects = MyInterface->GetAllCurrentEffects();
						}

						//���� ������ CurrentEffects �� ������
						if (CurrentEffects.Num() > 0)
						{
							//�������� �� ������� ��� �������, ��� ���� � false
							while (j < CurrentEffects.Num() && !bCanAddEffect)
							{
								//���� � ������� ��� ������� ����������� ����, ������� �� �������� �������� � ������ ������
								//�� ������ ���� �� true
								if (CurrentEffects[j]->GetClass() != AddStatusEffectClass)
								{
									bCanAddEffect = true;
								}

								j++;
							}
						}
						//���� ��� ������ ������ � �������
						else
						{
							//����� ������ ���� �� true
							bCanAddEffect = true;
						}


					}
					//���� � ������� � BP �������, ��� �� ����� ���� ������� ��������� ���
					else
					{
						bCanAddEffect = true;
					}
					

					//�������� �����
					if (bCanAddEffect)
					{
					
						//bHavePossibleSurface = true;
						//������ ������������� ������� ������ ����� ������������ Hit.GetActor()
						// 
						//������ ������������� ������� � ��������� Class
						UMyTDSGame_StatusEffect* NewStatusEffect = NewObject<UMyTDSGame_StatusEffect>(TakeEffectActor, AddStatusEffectClass);

						if (NewStatusEffect)
						{
							NewStatusEffect->InitializeObject(TakeEffectActor, NameBoneHit);
						}
					
					}
					
				}
				i++;
			}
		}
	}
}