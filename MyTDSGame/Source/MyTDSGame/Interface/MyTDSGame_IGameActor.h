// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "../Game/MyTDSGame_StatusEffect.h"
#include "../FunctionLibrary/Types.h"

#include "MyTDSGame_IGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UMyTDSGame_IGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MYTDSGAME_API IMyTDSGame_IGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
/*
	//���������� ������ ��� ����������	
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Event")
		void AvailableForEffectsBP();

	//NativeEvent ����� ���������� ��� ������������� CPP ��� � ���������
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Event")
		bool AvailableForEffects(); // ��� ��� ��������, ��� ������� Callable � NativeEvent ���������� � �PP ����� ��������� �� �����������
*/

	//����� ������� ������� ����� ��������� ���� Interface � ������������ �� Override
	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UMyTDSGame_StatusEffect*> GetAllCurrentEffects();

	virtual void RemoveEffect(UMyTDSGame_StatusEffect* RemoveEffect);

	virtual void AddEffect(UMyTDSGame_StatusEffect* AddEffect);

	//������� �����

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void DropWeaponToWorld(FDropItem DropItemInfo);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Cout);
};
